from typing import List, Tuple

from celery.utils.log import get_task_logger
from rdflib import URIRef
from rdflib.namespace import XSD
from n4e_kh_schema_py.n4eschema import MetadataStandard

from kh_populator.celery_deploy import app, HARVESTING_DELAY
from kh_populator.util import (
    Sources,
    RE3DATA_SOURCE_SYSTEM,
    ROR_SOURCE_SYSTEM,
    get_id,
)

from kh_populator.harvesting_consumers.ror_consumer import upsert as ror_upsert
from kh_populator.n4e_task import N4ETask

from kh_populator.targets.target_handler import ObjectTypeChoice
from kh_populator_domain import re3data, dcc
from kh_populator_domain.rdamsc import DCC_TO_RDA_MSC
from kh_populator_logic.rdf import (
    FOAF,
    N4E,
    OWL,
    jsonld_dict_to_metadata_object,
)

log = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.RE3DATA)
def upsert(self: app.task, sourceID: str):
    """
    :param sourceID: the re3data ID of a repository/aggregator. Only the
        numerical ID, not the URL
    :return: The IRI (=KH ID) of the resource in the KH
    """
    catalog = re3data.harvest_catalog(
        sourceID,
    )
    if not catalog:
        # might be None in case that the repository is EOL according to re3data
        return
    # NOTE: It would be bettter to get the re3data DOI and use it as
    # sourceSystemID, but unfortunately the API currently dose not include
    # the DOI for the re3data record in the XML
    catalog.sourceSystem = RE3DATA_SOURCE_SYSTEM
    catalog.sourceSystemID = sourceID
    catalog.sourceSystemURL = re3data.RE3DATA_BASE_URL_UI + sourceID
    # now we try to map the organizations contained in the publisher
    # property to entities in the KH
    needs_to_harvest_from_ROR_first = False
    for i, organization in enumerate(catalog.publisher):
        if organization.hasRorId:
            organization_id = get_id(ROR_SOURCE_SYSTEM, organization.hasRorId)
            if self.target_handler.exists(organization_id):
                catalog.publisher[i] = organization_id
            else:
                log.info("Sending to ROR queue: " + organization.hasRorId)
                ror_upsert.apply_async(
                    [organization.hasRorId, True], queue=Sources.ROR.name
                )
                needs_to_harvest_from_ROR_first = True
        else:
            predicate_objects: List[Tuple[str, str, ObjectTypeChoice]] = []
            for name in organization.name:
                if name.language:
                    lang = name.language
                    predicate_objects.append(
                        # predicate_objects argument can be a list of 2-/3-/4
                        # length tuples, but typing this would be very
                        # unreadable, so ignore:
                        ("foaf:name", str(name), "lang_literal", lang)
                    )
                else:
                    predicate_objects.append(
                        ("foaf:name", str(name), "literal")
                    )
            if organization.homepage:
                for homepage in organization.homepage:
                    predicate_objects.append(
                        ("foaf:homepage", homepage, "uri")
                    )
            namespaces = {"foaf": FOAF, "n4e": N4E}
            existing_orga_iris = (
                self.target_handler.get_iri_by_predicates_objects(
                    predicate_objects,
                    organization.class_class_uri,
                    namespaces,
                )
            )
            if len(existing_orga_iris) == 1:
                catalog.publisher[i] = existing_orga_iris[0]
    if needs_to_harvest_from_ROR_first:
        upsert.apply_async(
            [sourceID],
            queue=Sources.RE3DATA.name,
            countdown=HARVESTING_DELAY,
        )
        return

    metadata_standard_iris = []
    for metadata_standard in catalog.supportsMetadataStandard:
        specification = metadata_standard.hasDocument
        website = metadata_standard.hasWebsite
        dcc_specification = ""
        namespaces = {"n4e": N4E, "owl": OWL}
        if specification.startswith("http://www.dcc.ac.uk/resources/"):
            dcc_specification = specification
            predicate_objects = [
                ("owl:sameAs", str(specification), "uri"),
                ("n4e:hasDocument", str(specification), "uri"),
            ]
            iris = self.target_handler.get_iri_by_predicates_objects(
                predicate_objects,
                MetadataStandard.class_class_uri,
                namespaces,
                operator="OR",
            )
            if len(iris) == 1:
                metadata_standard_iris.append(iris[0])
                continue
            # if it could not be found via existing owl:sameAs mapping
            # try to search via specification and website URL
            metadatastandard_dcc = dcc.parse_dcc_resource_standard(
                specification
            )
            if metadatastandard_dcc is not None:
                specification = metadatastandard_dcc.hasDocument
                metadatastandard = metadatastandard_dcc
                website = metadatastandard_dcc.hasWebsite
        predicate_objects = []
        if specification:
            predicate_objects.append(
                (
                    "n4e:hasDocument",
                    specification,
                    "literal_typed",
                    XSD.anyURI,
                )
            )
        if website:
            predicate_objects.append(
                ("n4e:hasWebsite", website, "literal_typed", XSD.anyURI)
            )
        if predicate_objects:
            metadatastandard_iris = (
                self.target_handler.get_iri_by_predicates_objects(
                    predicate_objects,
                    MetadataStandard.class_class_uri,
                    namespaces,
                    operator="OR",
                )
            )
            if len(metadatastandard_iris) == 0:
                if str(dcc_specification) in DCC_TO_RDA_MSC:
                    predicate_objects_sourcesystemid = [
                        (
                            "n4e:sourceSystemID",
                            DCC_TO_RDA_MSC[str(dcc_specification)],
                        )
                    ]
                    metadatastandard_iris = (
                        self.target_handler.get_iri_by_predicates_objects(
                            predicate_objects_sourcesystemid,
                            MetadataStandard.class_class_uri,
                            namespaces,
                        )
                    )
            if len(metadatastandard_iris) == 1:
                metadatastandard_id = metadatastandard_iris[0]
                if dcc_specification:
                    jsonldrecord = self.target_handler.get_as_jsonld_frame(
                        metadatastandard_id
                    )
                    metadatastandard = jsonld_dict_to_metadata_object(
                        jsonldrecord, MetadataStandard
                    )
                    if (
                        URIRef(dcc_specification)
                        not in metadatastandard.sameAs
                    ):
                        metadatastandard.sameAs.append(
                            URIRef(dcc_specification)
                        )
                        id_ = metadatastandard.id
                        log.info(
                            f"Update: {id_} owl:sameAs {dcc_specification}"
                        )
                        self.target_handler.update(metadatastandard)
                metadata_standard_iris.append(metadatastandard_id)
                continue
            elif len(metadatastandard_iris) > 1:
                log.warning(
                    "Could not uniquely match metadatastandard: ",
                    predicate_objects,
                )
        log.warning(
            "Failed to parse metadata standard %s from re3data object %s"
            % (metadata_standard, sourceID)
        )
    catalog.supportsMetadataStandard = metadata_standard_iris

    iri = get_id(
        RE3DATA_SOURCE_SYSTEM,
        sourceID,
    )
    catalog.id = iri
    object_ = self.target_handler.get_as_jsonld_frame(iri)
    if object_ is None:
        # re3data catalog entry does not exist yet in the KH, create it
        iri = self.target_handler.create(catalog)
    else:
        catalog_previous = jsonld_dict_to_metadata_object(
            object_, type(catalog)
        )
        if catalog_previous.spatialCoverage:
            catalog.spatialCoverage = catalog_previous.spatialCoverage
        self.target_handler.update(catalog)
    return iri
