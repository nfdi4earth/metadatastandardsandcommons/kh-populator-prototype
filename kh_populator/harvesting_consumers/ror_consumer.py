import requests
from celery.utils.log import get_task_logger
from rdflib import URIRef
from n4e_kh_schema_py.n4eschema import Organization

from kh_populator.celery_deploy import app
from kh_populator.util import Sources, ROR_SOURCE_SYSTEM, get_id
from kh_populator.n4e_task import N4ETask
from kh_populator.harvesting_consumers import wikidata_consumer
from kh_populator_domain.ror import (
    BASE_URL,
    BASE_URL_UI,
    parse_organization_dict,
    ror_url_to_id_str,
)
from kh_populator_domain.wikidata import (
    WD_ENTITY_BASE_URL,
    wikidata_uri_to_id_str,
)
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object

logger = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.ROR)
def upsert(self: app.task, sourceID: str, create_only: bool = False):
    """
    :param sourceID: the ROR ID of the organization upsert
    :return: The IRI (=KH ID) of the resource in the KH
    """
    ror_id = sourceID.strip()
    ror_id = ror_id.lstrip("https://ror.org/")
    ror_id = ror_id.lstrip("http://ror.org/")

    organization_kh_iri = get_id(ROR_SOURCE_SYSTEM, ror_id)
    kh_object_ = self.target_handler.get_as_jsonld_frame(organization_kh_iri)
    if create_only and kh_object_ is not None:
        return organization_kh_iri
    response = requests.get(BASE_URL + ror_id, timeout=10)
    if response.status_code != 200:
        # TODO: need to create ticket that found invalid ROR ID in source
        print("Could not retrieve from ROR: " + ror_id)
        # pdb.set_trace()
        return None
    # TODO: handle timeout error as new push to queue with failed count
    ror_dict = response.json()
    organization = parse_organization_dict(ror_dict)
    organization.sourceSystem = ROR_SOURCE_SYSTEM
    organization.sourceSystemID = ror_id
    organization.sourceSystemURL = BASE_URL_UI + ror_id
    organization.id = organization_kh_iri

    if "relationships" in ror_dict:
        for relationship in ror_dict["relationships"]:
            if relationship["type"] == "Parent":
                parent_ror_id = ror_url_to_id_str(relationship["id"])
                parent_iri = get_id(ROR_SOURCE_SYSTEM, parent_ror_id)
                if not self.target_handler.exists(parent_iri):
                    upsert.apply_async([parent_ror_id], queue=Sources.ROR.name)
                    upsert.apply_async(
                        [sourceID], queue=Sources.ROR.name, countdown=60
                    )
                    return
                organization.subOrganizationOf.append(URIRef(parent_iri))

    if kh_object_ is None:
        self.target_handler.create(organization)
    else:
        # handle (do not overwrite) that other harvesters (e.g.
        # Wikidata) can also edit certain attributes of the
        # organization
        current_kh_orga = jsonld_dict_to_metadata_object(
            kh_object_, Organization
        )
        for name in current_kh_orga.name:
            if name not in organization.name:
                organization.name.append(name)
        organization.hasNFDI4EarthContactPerson = (
            current_kh_orga.hasNFDI4EarthContactPerson
        )
        organization.hasMembership = current_kh_orga.hasMembership
        organization.hasLogo = current_kh_orga.hasLogo
        if current_kh_orga.hasGeometry:
            organization.hasGeometry = current_kh_orga.hasGeometry
        if current_kh_orga.altLabel:
            organization.altLabel = current_kh_orga.altLabel
        organization.id = organization_kh_iri
        organization_kh_iri = self.target_handler.update(organization)

    # After every upsert of an ROR organization, run the Wikidata augmentation
    # which adds a logo and can add an English name if none of the names in ROR
    # was marked as being in English
    for other_identifier in organization.sameAs:
        if other_identifier.startswith(WD_ENTITY_BASE_URL):
            wikidata_id = wikidata_uri_to_id_str(other_identifier)
            # wait 60 sec before running the augmentation in order not to
            # overload KH server
            wikidata_consumer.augment_organization.apply_async(
                [wikidata_id, organization_kh_iri],
                queue=Sources.WIKIDATA.name,
                countdown=60,
            )
