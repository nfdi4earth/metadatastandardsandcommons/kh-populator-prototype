from celery.utils.log import get_task_logger
from rdflib import URIRef, BNode

from n4e_kh_schema_py.n4eschema import SoftwareSourceCode, Person, Organization

from kh_populator.celery_deploy import app
from kh_populator.util import (
    RSD_SOURCE_SYSTEM1,
    RSD_SOURCE_SYSTEM2,
    Sources,
    get_id,
)
from kh_populator.n4e_task import N4ETask
from kh_populator_logic.rdf import license_id_to_url_based_on_spdx

log = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.RSD1)
def upsert_rsd1(self: app.task, rsd_software: dict):
    """ """
    source_system_iri = RSD_SOURCE_SYSTEM1
    base_url = "https://research-software-directory.org/"
    upsert(rsd_software, source_system_iri, base_url, self.target_handler)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.RSD2)
def upsert_rsd2(self: app.task, rsd_software: dict):
    """ """
    source_system_iri = RSD_SOURCE_SYSTEM2
    base_url = "https://helmholtz.software/"
    upsert(rsd_software, source_system_iri, base_url, self.target_handler)


def upsert(rsd_software, source_system_iri, base_url, target_handler):
    """
    This function receives a dict of a software instance as from the RSD,
    turns it into a KH metadata object and upserts the instance in
    the backend (target_handler).
    """
    iri = get_id(source_system_iri, rsd_software["id"])
    software = rsd_software_to_kh_software(
        rsd_software,
        base_url,
    )
    software.id = iri
    software.sourceSystem = source_system_iri
    software.sourceSystemID = rsd_software["id"]
    software.sourceSystemURL = base_url + "software/" + rsd_software["slug"]

    if not target_handler.exists(iri):
        log.debug(
            f"Software {software['name']} does not exist yet in the KH from "
            + f"source {source_system_iri} with source "
            + f"ID {rsd_software['id']}",
        )
        new_iri = target_handler.create(software)
        log.info(
            "Created new resource in KH for software %s: %s",
            rsd_software["brand_name"],
            new_iri,
        )
    else:
        log.debug(
            "Overwriting existing KH entry for: %s %s",
            source_system_iri,
            rsd_software["id"],
        )
        ng_iri = target_handler.update(software)
        log.info(
            "Updated resource in KH for software %s: %s",
            rsd_software["brand_name"],
            ng_iri,
        )


def rsd_software_to_kh_software(
    rsd_software: dict,
    url: str,
) -> SoftwareSourceCode:
    software = SoftwareSourceCode(
        id="tmp",
        # sourceSystemID and sourceSystem set outside of this function
        codeRepository=URIRef(rsd_software["repository_url"]["url"]),
        description=rsd_software[
            "description"
        ],  # rsd_software["short_statement"],
        name=rsd_software["brand_name"],
        url=rsd_software["get_started_url"],
    )
    license_str = rsd_software["repository_url"]["license"]
    license_uri = license_id_to_url_based_on_spdx(license_str)
    if license_uri:
        software.license = license_uri
    else:
        software.copyrightNotice = f"License info: {license_str}"

    identifiers = []
    if rsd_software["concept_doi"]:
        identifiers.append("https://doi.org/" + rsd_software["concept_doi"])
    identifiers.append(url + "software/" + rsd_software["slug"])
    identifiers.append(rsd_software["id"])
    for i in identifiers:
        software.identifier.append(i)

    for k in rsd_software["keyword"]:
        software.keywords.append(k["value"])

    if rsd_software["repository_url"]["languages"]:
        for lang in rsd_software["repository_url"]["languages"]:
            software.programmingLanguage.append(lang)

    for c in rsd_software["contributor"]:
        contributor = Person(
            id=BNode(), name=c["given_names"] + " " + c["family_names"]
        )
        if c["affiliation"]:
            orga = Organization(
                id=BNode(),
                name=c["affiliation"],
            )
            contributor.affiliation.append(orga)
        if c["email_address"]:
            contributor.email.append(c["email_address"])
        if c["orcid"]:
            contributor.orcidId = c["orcid"]
        contributor.email

        software.contributor.append(contributor)

    # TODO contributing organisations (see relation software_for_organisation)
    """
    orga_count = 0
    for o in rsd_software["organisation"]:
        if o["ror_id"]:  # only add orgs with ROR ID
            orga_iri, orga = get_or_create_organization(
                o["ror_id"], target_handler
            )
            software.contributor.append(orga_iri)
            orga_count += 1
        else:
            log.warning(
                "Not adding organisation because it has no ROR ID: %s",
                o["name"],
            )
    log.info(
        "Added %s individual contributors and %s organisations to software %s",
        len(rsd_software["contributor"]),
        orga_count,
        rsd_software["brand_name"],
    )
    """

    return software
