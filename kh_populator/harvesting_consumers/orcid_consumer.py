from celery.utils.log import get_task_logger

from n4e_kh_schema_py.n4eschema import Person
from kh_populator.celery_deploy import app
from kh_populator.util import ORCID_SOURCE_SYSTEM, Sources, get_id
from kh_populator.n4e_task import N4ETask
from kh_populator_domain import orcid
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object

logger = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.ORCID)
def upsert(self: app.task, sourceID: str):
    """
    :param sourceID: the ORCID of the person to upsert
    :return: The IRI (=KH ID) of the resource in the KH
    """
    print("ORCID consumer received: " + sourceID)
    person = orcid.get_person_from_schemardf(sourceID)
    person.sourceSystem = ORCID_SOURCE_SYSTEM
    person.sourceSystemID = sourceID
    person.sourceSystemURL = orcid.ORCID_BASE_URL + sourceID
    person_kh_iri = get_id(
        ORCID_SOURCE_SYSTEM,
        sourceID,
    )
    person.id = person_kh_iri
    kh_object_ = self.target_handler.get_as_jsonld_frame(person_kh_iri)
    if kh_object_ is None:
        self.target_handler.create(person)
    else:
        # handle (do not overwrite) that other harvesters
        # might have added information fields
        current_kh_object = jsonld_dict_to_metadata_object(kh_object_, Person)
        if current_kh_object.email:
            person.email = current_kh_object.email
        self.target_handler.update(person)
