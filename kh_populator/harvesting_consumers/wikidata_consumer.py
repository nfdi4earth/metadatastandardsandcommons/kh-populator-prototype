"""
This modules provides both a function for harvesting metadata of an
organization from Wikidata into the KH, and for augmenting an existing
organization metadata object in the KH (that was harvested e.g. from ror.org).

Currently augments the following organization metadata from Wikidata:
    - hasLogo
    - name/altLabel (if no English name was available from ROR)
    - hasGeometry -> coordinates, if available (because coordinates in ROR
      are very coarse on level of detail of the city)

NOTE: Since vcard:hasLogo currently as a cardinality of 1 in the data model,
this property gets overwritten on the respective KH object.
TODO: Check if this above is the desired way to continue.
"""

from celery.utils.log import get_task_logger
from geomet import wkt
from rdflib import URIRef, Literal

from n4e_kh_schema_py.n4eschema import Organization, Geometry

from kh_populator.celery_deploy import (
    app,
)
from kh_populator.util import WIKIDATA_SOURCE_SYSTEM, Sources, get_id
from kh_populator.n4e_task import N4ETask
from kh_populator_domain import wikidata
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object, GEO

logger = get_task_logger(__name__)


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.WIKIDATA)
def upsert(self: app.task, sourceID: str):
    """
    :param sourceID: the Wikidata ID of the organization to upsert
    :return: The IRI (=KH ID) of the resource in the KH
    """
    organization = wikidata.harvest_organization(sourceID)
    organization.sourceSystem = WIKIDATA_SOURCE_SYSTEM
    organization.sourceSystemID = sourceID
    organization.sourceSystemURL = wikidata.WD_ENTITY_BASE_URL + sourceID
    organization_kh_iri = get_id(WIKIDATA_SOURCE_SYSTEM, sourceID)
    organization.id = organization_kh_iri
    if not self.target_handler.exists(organization_kh_iri):
        organization_kh_iri = self.target_handler.create(organization)
    else:
        # handle (do not overwrite) that other harvesters (e.g.
        # NFDI4Earth collected data) can also edit certain attributes of the
        # organization
        organization.id = organization_kh_iri
        current_jsonld = self.target_handler.get_as_jsonld_frame(
            organization_kh_iri
        )
        current_kh_orga = jsonld_dict_to_metadata_object(
            current_jsonld, Organization
        )
        organization.hasNFDI4EarthContactPerson = (
            current_kh_orga.hasNFDI4EarthContactPerson
        )
        organization.hasMembership = current_kh_orga.hasMembership
        organization_kh_iri = self.target_handler.update(organization)
    return organization_kh_iri


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.WIKIDATA)
def augment_organization(
    self: app.task,
    sourceID: str,
    organization_kh_iri: URIRef,
):
    """
    :param sourceID: the Wikidata ID of the organization to upsert
    :return: The IRI (=KH ID) of the resource in the KH
    """
    wikidata_url = wikidata.wikidata_id_to_uri(sourceID)
    query = wikidata.organization_augment_template.replace(
        "TEMPLATE_IRI", wikidata_url
    )
    wikidata_response = wikidata.execute_query(query, "application/json")
    if wikidata_response.status_code == 200:
        logo_url = ""
        labels = []
        geoms = []
        for binding in wikidata_response.json()["results"]["bindings"]:
            if "logoURL" in binding:
                logo_url = binding["logoURL"]["value"]
                logger.warning("Augmenting called " + logo_url)
            if "wikidata_label_en" in binding:
                labels.append(
                    Literal(binding["wikidata_label_en"]["value"], lang="en")
                )
            if "coordinateLocation" in binding:
                node = binding["coordinateLocation"]
                if node["datatype"] == str(GEO.wktLiteral):
                    geojson = wkt.loads(node["value"].upper())
                    wkt_literal = Literal(
                        wkt.dumps(geojson, decimals=6), datatype=GEO.wktLiteral
                    )
                    geoms.append(Geometry(asWKT=wkt_literal))
        if not (logo_url or labels or geoms):
            return  # Nothing to do
        data_dict = self.target_handler.get_as_jsonld_frame(
            organization_kh_iri
        )
        organization: Organization = jsonld_dict_to_metadata_object(
            data_dict, Organization
        )
        do_update = False
        if len(logo_url) > 0:
            do_update = True
            organization.hasLogo = logo_url

        if len(labels) > 0:
            existing_names_en = [
                n for n in organization.name if n.language == "en"
            ]
            if len(existing_names_en) == 0:
                do_update = True
                organization.name.append(labels.pop(0))
            for label in labels:
                if label not in organization.altLabel:
                    do_update = True
                    organization.altLabel.append(label)
        if len(geoms) > 0:
            organization.hasGeometry = geoms
        if do_update:
            self.target_handler.update(organization)
    elif wikidata_response.status_code == 429:
        logger.warning(
            "Wikidata augment query received code 429 - too many requests"
        )
        logger.warning(wikidata_response)
        logger.warning(wikidata_response.reason)
        logger.warning(wikidata_response.headers)
        logger.warning(wikidata_response.text)
        # TODO: add this item again to the queue but with wait time > 2 min
        # and consider setting a global flag that currently Wikidata should
        # not be queried
