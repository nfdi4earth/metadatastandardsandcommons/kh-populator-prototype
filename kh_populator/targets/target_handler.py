"""An abstract class for target KH handlers"""

from abc import ABC, abstractmethod
from celery.utils.log import get_task_logger
from linkml_runtime.utils.yamlutils import YAMLRoot
from rdflib import URIRef, Namespace
from typing import Union, List, Tuple, Optional, Dict, Literal

from n4e_kh_schema_py.n4eschema import Organization
from kh_populator_logic.rdf import ns_rdf, DCAT, DCT, FOAF, SKOS

log = get_task_logger(__name__)

ObjectTypeChoice = Literal["literal", "uri", "lang_literal", "literal_typed"]


class TargetHandler(ABC):
    """
    Abstract super class which defines methods that all subclasses must
    have
    """

    def __init__(self):
        pass

    @abstractmethod
    def create(self, metadata_object: YAMLRoot) -> URIRef:
        pass

    @abstractmethod
    def update(self, metadata_object: YAMLRoot) -> URIRef:
        pass

    @abstractmethod
    def delete(self, object_iri: URIRef):
        pass

    @abstractmethod
    def exists(self, object_iri: URIRef) -> bool:
        pass

    @abstractmethod
    def delete_all_from_source_system(
        self, source_system: URIRef, class_uri: URIRef = URIRef("")
    ):
        pass

    @abstractmethod
    def get_iri_by_predicates_objects(
        self,
        predicate_objects: Union[
            List[
                Tuple[
                    str,
                    str,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                    str,
                ]
            ],
        ],
        rdf_type: URIRef,
        namespaces: Dict[str, Namespace],
        operator: Literal["AND", "OR"] = "AND",
    ) -> List[URIRef]:
        pass

    @abstractmethod
    def get_as_jsonld_frame(self, iri: URIRef) -> Optional[Dict]:
        pass

    @abstractmethod
    def disable_validation_for_initial_objects(self):
        pass

    @abstractmethod
    def reenable_data_validation(self):
        pass

    @abstractmethod
    def execute_query(self, query: str) -> List:
        """
        :param query: The SPARQL query to execute against the graph.
        :return: The query result, serialized to JSON as in
            http://www.w3.org/TR/rdf-sparql-json-res/
        """
        pass

    def predicates_objects_to_sparql_query(
        self,
        predicate_objects: Union[
            List[
                Tuple[
                    str,
                    str,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                ]
            ],
            List[
                Tuple[
                    str,
                    str,
                    ObjectTypeChoice,
                    str,
                ]
            ],
        ],
        rdf_type: URIRef,
        namespaces: Dict[str, Namespace],
        operator: Literal["AND", "OR"] = "AND",
    ) -> str:
        """
        NOTE: The type hint is not completly correct, as it suggests that
        predicate_objects should be a list of uniform tuples, but actually
        2-/3-/4- length tuples can be mixed.
        NOTE: The query variable s in the SPARQL query cannot be changed
        because the subclass Cordra.py depends on that name.
        """
        namespaces.update({"rdf": Namespace(ns_rdf)})
        query = ""
        for key, value in namespaces.items():
            query += f"PREFIX {key}: <{value}> \n"
        query += """

        SELECT DISTINCT ?s
        WHERE {
        """
        if operator == "AND":
            query += f" ?s rdf:type <{rdf_type}> ."
        for i, tuple_ in enumerate(predicate_objects):
            predicate = tuple_[0]
            object_ = tuple_[1]
            if operator == "OR":
                query += f"{{ ?s rdf:type <{rdf_type}> . \n"
            query += f"\n ?s {predicate} "
            if len(tuple_) > 2:
                datatype = tuple_[2]
                if datatype == "literal" or datatype == "lang_literal":
                    object_ = object_.replace('"', '\\"')
                if datatype == "literal":
                    query += f'"{object_}"'
                elif datatype == "uri":
                    query += f"<{object_}>"
                elif datatype == "lang_literal":
                    if len(tuple_) > 3:
                        lang_tag = tuple_[3]
                        query += f'"{object_}"@{lang_tag}'
                    else:
                        query += f'"{object_}"'
                elif datatype == "literal_typed":
                    if len(tuple_) > 3:
                        literal_datatype = tuple_[3]
                        query += f'"{object_}"^^<{literal_datatype}>'
                    else:
                        query += f'"{object_}"'
            else:
                query += f'"{object_}"'
            query += " ."
            if operator == "OR":
                if i < len(predicate_objects) - 1:
                    query += "} UNION "
                else:
                    query += "}"
        query += "\n }"
        return query

    def catalogs_with_csw_endpoints_query(self) -> str:
        """
        NOTE: range of dct:conformsTo in KH datamodel is currently
        set to Literal, but might change to rdfs:Resource (URI Ref)
        """
        query = f"""
        PREFIX dcat: <{DCAT}>
        PREFIX dct: <{DCT}>
        """
        query += """
        SELECT ?s
        WHERE {
            ?s dcat:distribution ?d .
            ?d dct:conformsTo "http://www.opengis.net/def/serviceType/ogc/csw".
            ?d dcat:accessURL ?accessURL .
        }
        """
        return query

    def find_organization_by_name(self, orga_name: str) -> Union[URIRef, None]:
        orga_type: URIRef = Organization.class_class_uri
        predicates_objects = [("foaf:name", orga_name)]
        namespaces = {"foaf": FOAF}
        orga_iris = self.get_iri_by_predicates_objects(
            predicates_objects, orga_type, namespaces
        )
        if len(orga_iris) == 1:
            return orga_iris[0]
        predicates_objects = [("skos:altLabel", orga_name)]
        namespaces = {"skos": SKOS}
        orga_iris = self.get_iri_by_predicates_objects(
            predicates_objects, orga_type, namespaces
        )
        if len(orga_iris) == 1:
            print("--- found mapping via altLabel for " + orga_name)
            return orga_iris[0]
        elif len(orga_iris) > 1:
            log.warning(
                "Failed to uniquely match organization", predicates_objects
            )
        else:
            return None

    def log_upsert(self, action: str, metadata_object: YAMLRoot):
        base_log_msg = f"Will {action} on target: %s"
        has_logged = False
        for name_attr in ["title", "name"]:
            if hasattr(metadata_object, name_attr):
                log.debug(base_log_msg, getattr(metadata_object, name_attr))
                has_logged = True
                break
        if not has_logged:
            log.info(base_log_msg, metadata_object)

    def find_all_from_source_system_query(
        self, source_system: URIRef, class_uri: URIRef = URIRef("")
    ):
        query = (
            """
        PREFIX n4e: <http://nfdi4earth.de/ontology/>
        SELECT * {
        ?s n4e:sourceSystem <%s> .
        """
            % source_system
        )
        if class_uri:
            query += "\n ?s a <%s>" % class_uri
        query += "\n }"
        return query
