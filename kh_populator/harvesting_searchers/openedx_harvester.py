"""
Function which harvests course metadata from NFDI4Earths's openedx instance
"""

import logging
import requests
from bs4 import BeautifulSoup
from rdflib import URIRef, BNode
import fnmatch

from n4e_kh_schema_py.n4eschema import (
    Person,
    LearningResource,
    EducationalLevel,
    LearningResourceType,
)
from kh_populator.celery_deploy import app
from kh_populator.util import OPENEDX_SOURCE_SYSTEM, get_id
from kh_populator.n4e_task import N4ETask
from kh_populator.util import config, Sources
from kh_populator_logic.rdf import DFGFO, get_spdx_iri_from_license_url

logger = logging.getLogger(__name__)
url_base = config("openedx", "url_base")


def request(url_sub=None, headers={}, data={}, method="get"):
    _method = getattr(requests.sessions.Session(), method)
    return _method(url_base + url_sub, headers=headers, data=data)


def get_token_headers():
    client_id = config("openedx", "client_id")
    client_secret = config("openedx", "client_secret")
    client_token = None
    data = {
        "grant_type": "client_credentials",
        "client_id": client_id,
        "client_secret": client_secret,
        "token_type": "jwt",
    }
    response = request(
        url_sub="oauth2/access_token", headers=None, data=data, method="post"
    )
    if response.ok:
        client_token = response.json()["access_token"]
        return {
            "Authorization": "JWT %s" % client_token,
        }
    else:
        raise ValueError(
            "Unable to get OpenEdX token, got %d: %s"
            % (response.status_code, response.text)
        )


def get_courses(headers: dict) -> list:
    courses = []
    response = request(url_sub="api/courses/v1/courses/", headers=headers)
    if response.ok:
        courses = response.json()["results"]
    else:
        print(response.status_code, response.text)
    return courses


@app.task(base=N4ETask, bind=True, source_system_queue=Sources.OPENEDX)
def harvest(self: app.task):
    headers = get_token_headers()
    courses = get_courses(headers)
    courses_dict = {course["id"]: course for course in courses}

    def upsert_course(course, prerequisite_courses_graph):
        block = {}
        response_blocks = request(
            url_sub="api/courses/v1/courses/%s/blocks" % course["id"],
            headers=headers,
        )
        if response_blocks.ok:
            block = response_blocks.json()

        learning_resource = api_object_to_learning_resource(course, block)
        if learning_resource is None:
            return

        iri = get_id(
            OPENEDX_SOURCE_SYSTEM,
            learning_resource.sourceSystemID,
        )
        learning_resource.id = iri
        approved_course_prerequisites_n4e_ids = []
        for i in range(len(learning_resource.coursePrerequisites)):
            preq_learning_resource_id = learning_resource.coursePrerequisites[
                i
            ]
            preq_n4e_iri = get_id(
                OPENEDX_SOURCE_SYSTEM, preq_learning_resource_id
            )
            if not self.target_handler.exists(preq_n4e_iri):
                if preq_learning_resource_id in courses_dict:
                    if (
                        preq_learning_resource_id
                        not in prerequisite_courses_graph
                    ):
                        prerequisite_courses_graph.append(course["id"])
                        upsert_course(
                            courses_dict[preq_learning_resource_id],
                            prerequisite_courses_graph,
                        )
                        if self.target_handler.exists(preq_n4e_iri):
                            approved_course_prerequisites_n4e_ids.append(
                                preq_n4e_iri
                            )
                    else:
                        logging.warning(
                            f"{course['id']} can not set coursePrerequisite "
                            + f"{preq_learning_resource_id}. "
                            + "Circular reference!"
                        )
                else:
                    logging.warning(
                        f"{course['id']} can not set coursePrerequisite "
                        + f"{preq_learning_resource_id}. "
                        + "Reference course not found!"
                    )
            else:
                approved_course_prerequisites_n4e_ids.append(preq_n4e_iri)
        learning_resource.coursePrerequisites = (
            approved_course_prerequisites_n4e_ids
        )

        learning_resource.sourceSystem = OPENEDX_SOURCE_SYSTEM
        if not self.target_handler.exists(iri):
            # learning resource does not exist yet in the KH, create it
            self.target_handler.create(learning_resource)
        else:
            # learning resource does exist already in the KH
            self.target_handler.update(learning_resource)

    for course in courses:
        # initially set prerequisite_courses_graph to empty
        upsert_course(course, [])


def api_object_to_learning_resource(
    course: dict, block: dict
) -> LearningResource:
    learning_resource = LearningResource(id="tmp", name=course["name"])
    learning_resource.description = [course["short_description"]]
    course_id = course["id"]
    # URL
    learning_resource.url = [url_base + "courses/" + course_id]
    learning_resource.sourceSystemURL = url_base + "courses/" + course_id
    # id
    learning_resource.identifier = [course_id]
    learning_resource.sourceSystemID = course_id
    # default keywords
    learning_resource.keywords = ["education", "training", "tutorial"]

    if "overview" in block:
        soup = BeautifulSoup(block["overview"], features="html.parser")

        # If no meta tag available (i.e. course under development),
        # return nothing
        if soup.find("meta") is None:
            return None
        else:
            # meta tags: author, subjectarea
            for mt in soup.select("meta"):

                if mt["name"] == "author":
                    if "|" in mt["content"]:
                        author_name = mt["content"].split("|")[0]
                        author_iri = mt["content"].split("|")[1]
                        if author_iri.startswith("https://orcid.org/"):
                            learning_resource.author.append(
                                Person(
                                    id=BNode(),
                                    url=URIRef(author_iri),
                                    name=author_name,
                                    orcidId=author_iri.split("/")[-1],
                                )
                            )
                        else:
                            learning_resource.author.append(
                                Person(
                                    id=BNode(),
                                    url=URIRef(author_iri),
                                    name=author_name,
                                )
                            )

                if mt["name"] == "subjectArea":
                    subject_areas_meta = mt["content"]
                    if subject_areas_meta:  # if not empty
                        for i in subject_areas_meta.split(","):
                            learning_resource.subjectArea.append(
                                URIRef(i.strip().replace("dfg:", DFGFO))
                            )

                if mt["name"] == "datePublished":
                    datep_meta = mt["content"]
                    if datep_meta:  # if not empty
                        learning_resource.datePublished = datep_meta.strip()

                if mt["name"] == "inLanguage":
                    language_name = mt["content"]
                    if language_name:  # if not empty
                        language_iri = URIRef(
                            "http://publications.europa.eu/resource/"
                            + "authority/language/"
                            + language_name.strip()
                        )
                        learning_resource.inLanguage.append(language_iri)

                if mt["name"] == "educationalLevel":
                    edulvl_raw = [i.strip() for i in mt["content"].split(",")]
                    if edulvl_raw:  # if not empty
                        # Make a list of permitted entries for educationalLevel
                        edulvl_permitted = [
                            "Intro",
                            "Beginner",
                            "Intermediate",
                            "Advanced",
                        ]
                        # Filter for correctly registered educational levels
                        edulvl_filtered = [
                            i for i in edulvl_raw if i in edulvl_permitted
                        ]
                        # Harvest this metadata
                        for i in edulvl_filtered:
                            learning_resource.educationalLevel.append(
                                EducationalLevel(i)
                            )

                if mt["name"] == "learningResourceType":
                    lrt_raw = [i.strip() for i in mt["content"].split(",")]
                    if lrt_raw:  # if not empty
                        # Make a list of permitted entries
                        # for learningResourceType
                        lrt_permitted = [
                            "exercise",
                            "slide",
                            "narrative text",
                            "quiz",
                            "video",
                        ]
                        # Filter for correctly registered
                        # learning resource types
                        lrt_filtered = [
                            i for i in lrt_raw if i in lrt_permitted
                        ]
                        # Harvest this metadata
                        for i in lrt_filtered:
                            learning_resource.learningResourceType.append(
                                LearningResourceType(i)
                            )

                if mt["name"] == "coursePrerequisites":
                    # Get source system id of the prerequisite course
                    preq_course_url = mt["content"].split("/")
                    preq_course_id = fnmatch.filter(
                        preq_course_url, "course*Self-Paced"
                    )
                    if preq_course_id:  # if not empty
                        preq_course_id = preq_course_id[0]
                        learning_resource.coursePrerequisites = [
                            preq_course_id
                        ]

                if mt["name"] == "keywords":
                    ls_keyw = [i.strip() for i in mt["content"].split(",")]
                    if ls_keyw:  # if list is not empty
                        learning_resource.keywords.extend(ls_keyw)

            # License is in the footer/p tag
            for i in soup.select(".license"):
                licenseurl = i.get("href")
                license_iri = get_spdx_iri_from_license_url(licenseurl)
                if license_iri:
                    learning_resource.license.append(license_iri)

            # Competency Required
            prerequisites = ""
            for prereq_p in soup.select(".competencyRequired"):
                for s in prereq_p.stripped_strings:
                    prerequisites += s
            if prerequisites:
                learning_resource.competencyRequired = [prerequisites.strip()]

    return learning_resource
