from kh_populator.celery_deploy import app
from kh_populator.util import Sources, RE3DATA_SOURCE_SYSTEM
from kh_populator.harvesting_consumers.re3data_consumer import upsert
from kh_populator.harvesting_searchers.khresources_updater import (
    get_all_harvested_ids_for_source_system,
)
from kh_populator.n4e_task import N4ETask
from kh_populator_domain import re3data


@app.task(base=N4ETask, bind=True)
def search(self: app.task):
    # Send search query to re3data to get IDs of all repositories which
    # are relevant for the NFDI4Earth
    re3data_repo_ids = re3data.get_harvestable_repository_ids()
    # Additionally retrieve IDs of all already harvested repositories from
    # the KH to detect if there is an object in the KH which does not exist
    # anymore in re3data.
    re3data_ids_in_kh = get_all_harvested_ids_for_source_system(
        self.target_handler, RE3DATA_SOURCE_SYSTEM
    )
    for re3data_id in re3data_ids_in_kh:
        if re3data_id not in re3data_repo_ids:
            re3data_repo_ids.append(re3data_id)
    for i, re3data_repo_id in enumerate(re3data_repo_ids):
        upsert.apply_async(
            [re3data_repo_id], queue=Sources.RE3DATA.name, countdown=i
        )
