import requests
from typing import List
from celery.utils.log import get_task_logger

from kh_populator.celery_deploy import app
from kh_populator.util import Sources
from kh_populator.harvesting_consumers.rsd_consumer import (
    upsert_rsd1,
    upsert_rsd2,
)
from kh_populator.n4e_task import N4ETask

log = get_task_logger(__name__)

REQUEST_TIMEOUT_SECONDS = 10

QUERY_FIELDS = (
    "*,repository_url!inner(url,languages,license),"
    + "keyword!inner(value),contributor!inner("
    + "given_names,family_names,affiliation,email_address,orcid"
    + "),organisation!inner(ror_id,name)"
)

# TODO: remove all '*' from queries and be a friendly harvester when the list
# of needed fields is fixed
SOFTWARE_QUERIES = [
    # software with projects (only RSD at eScience Center NL!) that have ERC
    #  research domains PE10 or SH7, see
    # https://erc.europa.eu/news/new-erc-panel-structure-2021-and-2022
    "/software?select="
    + QUERY_FIELDS
    + ",project!inner(*,research_domain!inner(key))"
    + "&project.research_domain.or=(key.eq.PE10,key.eq.SH7)",
    # full text search for 'earth'
    "/software?select=" + QUERY_FIELDS + "&description=fts.earth",
]

# keywords based on exploring the catalogues manually and searching the
# available keywords, e.g., with queries
# - https://research-software-directory.org/api/v1/keyword?value=imatch.geo
# - https://helmholtz.software/api/v1/keyword?value=imatch.earth
KEYWORD_MATCH_QUERIES = ["geo", "ocean", "earth"]
for kw in KEYWORD_MATCH_QUERIES:
    SOFTWARE_QUERIES.append(
        "/software?select=" + QUERY_FIELDS + "&keyword.value=imatch." + kw
    )

RSDS = {
    "https://research-software-directory.org/": {
        "endpoint": "api/v1",
        "email": "rsd@esciencecenter.nl",
        "ror": "00rbjv475",
        "harvestingkey": "RSD1",
    },
    "https://helmholtz.software/": {
        "endpoint": "api/v1",
        "email": "support@hifis.net",
        "ror": "0281dp749",
        "harvestingkey": "RSD2",
    },
}


def get_software_from_rsd_instance(request_url) -> List:
    log.debug("Harvesting URL: %s", request_url)
    response = requests.get(request_url, timeout=REQUEST_TIMEOUT_SECONDS)
    if response.status_code != 200:
        raise ValueError(
            "Error harvesting with request %s \n"
            + "Continueing with other queries.",
            request_url,
        )
    return response.json()


@app.task(base=N4ETask, bind=True)
def search(self: app.task):
    for url in RSDS.keys():
        log.info(
            "Harvesting %s%s as using %s queries",
            url,
            RSDS[url]["endpoint"],
            len(SOFTWARE_QUERIES),
        )

        # query software projects
        softwares = []
        for q in SOFTWARE_QUERIES:
            request_url = url + RSDS[url]["endpoint"] + q

            try:
                results = get_software_from_rsd_instance(request_url)
            except ValueError:
                continue
            if len(results) == 0:
                log.info("No results for query %s", request_url)
                continue

            log.debug("Query returned %s results", len(results))
            softwares += results

        log.info(
            "Finished queries, have %s pieces of software in the results",
            len(softwares),
        )

        # remove duplicates using dict comprehension by creating a set
        # of tuples with the id as
        softwares = {s["id"]: s for s in softwares}.values()
        log.info(
            "After duplicate removal, %s pieces of software were found in %s",
            len(softwares),
            url,
        )

        # iterate over all softwares and insert or update them
        for s in softwares:
            if url == "https://research-software-directory.org/":
                upsert_rsd1.apply_async([s], queue=Sources.RSD1.name)
            else:
                upsert_rsd2.apply_async([s], queue=Sources.RSD2.name)
