"""
This module contains functions for updating those resources in the KH
which are not explicitly updated during regular harvesting searches.

These are "dependent" resources, like organizations or persons which are
only imported into the KH because they are reference by a resource which
is harvested (like a repository, a dataset, ...)
"""

from datetime import datetime, timedelta
from rdflib import URIRef

from kh_populator.celery_deploy import app
from kh_populator.util import (
    Sources,
    ORCID_SOURCE_SYSTEM,
    ROR_SOURCE_SYSTEM,
    WIKIDATA_SOURCE_SYSTEM,
)
from kh_populator.harvesting_consumers import (
    ror_consumer,
    wikidata_consumer,
    orcid_consumer,
)
from kh_populator.n4e_task import N4ETask
from kh_populator.targets.target_handler import TargetHandler


def get_all_harvested_ids_for_source_system(
    target_handler: TargetHandler, source_system_uri: URIRef, days_back=2
):
    time_filter = datetime.utcnow() - timedelta(days=days_back)
    query = """
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX n4e: <http://nfdi4earth.de/ontology/>

    SELECT ?source_system_id {
        GRAPH ?g {
            ?kh_object n4e:sourceSystem <%s> .
            ?kh_object n4e:sourceSystemID ?source_system_id .
        }
        ?g n4e:lastSourceSync ?sync .
        FILTER (?sync < "%s"^^xsd:dateTime)
    }
    """ % (
        source_system_uri,
        time_filter.isoformat(),
    )
    bindings = target_handler.execute_query(query)
    return [b["source_system_id"]["value"] for b in bindings]


@app.task(base=N4ETask, bind=True)
def update_organizations_ror(self: app.task):
    for ror_id in get_all_harvested_ids_for_source_system(
        self.target_handler, ROR_SOURCE_SYSTEM
    ):
        ror_consumer.upsert.apply_async([ror_id], queue=Sources.ROR.name)


@app.task(base=N4ETask, bind=True)
def update_organizations_wikidata(self: app.task):
    for wikidata_id in get_all_harvested_ids_for_source_system(
        self.target_handler, WIKIDATA_SOURCE_SYSTEM
    ):
        wikidata_consumer.upsert.apply_async(
            [wikidata_id], queue=Sources.WIKIDATA.name
        )


@app.task(base=N4ETask, bind=True)
def update_persons_orcid(self: app.task):
    for orcid in get_all_harvested_ids_for_source_system(
        self.target_handler,
        ORCID_SOURCE_SYSTEM,
    ):
        orcid_consumer.upsert.apply_async([orcid], queue=Sources.ORCID.name)
