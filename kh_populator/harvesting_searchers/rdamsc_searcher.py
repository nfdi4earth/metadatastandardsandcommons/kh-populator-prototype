import requests

from kh_populator.celery_deploy import app
from kh_populator.util import Sources, RDA_MSC_SOURCE_SYSTEM
from kh_populator.harvesting_consumers.rdamsc_consumer import upsert
from kh_populator.harvesting_searchers.khresources_updater import (
    get_all_harvested_ids_for_source_system,
)
from kh_populator.n4e_task import N4ETask
from kh_populator_domain.rdamsc import (
    RDA_MSC_BASE_URL,
    check_if_parent_standard_fulfills_restrictions,
    check_if_standard_fulfills_restrictions,
)


@app.task(base=N4ETask, bind=True)
def search(self: app.task):
    # Send search query to re3data to get IDs of all repositories which
    # are relevant for the NFDI4Earth
    ids_to_be_harvested = []
    reached_max_count = False
    current_start = 1
    page_size = 20
    while not reached_max_count:
        response = requests.get(
            RDA_MSC_BASE_URL + f"m?pageSize={page_size}&start={current_start}",
            timeout=10,
        )
        data = response.json()["data"]
        current_start += page_size
        if current_start > data["totalItems"]:
            reached_max_count = True

        for standard in data["items"]:
            # check if the standard should be harvested into the KH based on
            # the research subject
            # "keywords" here is a controlled vocabulary of the research
            # subject the standard belong to, not to be confused with
            # dct:keyword in DCAT
            should_be_harvested = check_if_standard_fulfills_restrictions(
                standard
            )
            if not should_be_harvested:
                should_be_harvested = (
                    check_if_parent_standard_fulfills_restrictions(standard)
                )

            if should_be_harvested:
                ids_to_be_harvested.append(standard["mscid"])

    # Additionally retrieve IDs of all already harvested repositories from
    # the KH to detect if there is an object in the KH which does not exist
    # anymore in the RDA MSC.
    standard_ids_in_kh = get_all_harvested_ids_for_source_system(
        self.target_handler, RDA_MSC_SOURCE_SYSTEM
    )
    for rda_msc_id in standard_ids_in_kh:
        if rda_msc_id not in ids_to_be_harvested:
            ids_to_be_harvested.append(rda_msc_id)

    for id_ in ids_to_be_harvested:
        upsert.apply_async([id_], queue=Sources.RDAMSC.name)
