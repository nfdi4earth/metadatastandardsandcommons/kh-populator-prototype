"""
Function which harvests publications from zenodo.org uploaded by N4E

To handle different Zenodo resource versions, we use the "conceptdoi" as
sourceSystemId and during harvesting always resolve the conceptdoi to make
sure we harvest the latest version of a Zenodo resource.
"""

from kh_populator.celery_deploy import app
from kh_populator.util import Sources
from kh_populator.n4e_task import N4ETask
from kh_populator.harvesting_consumers.zenodo_consumer import upsert
from kh_populator_domain import zenodo

ZENODO_RE3DATA_ID = "r3d100010468"
ZENODO_DOI_PREFIX = "10.5281/"
ZENODO_OAI_PREFIX = "oai:zenodo.org:"


def get_harvesting_record_ids():
    zenodo_records = zenodo.fetch_records()
    return [
        zenodo.get_concept_doi_for_record(record) for record in zenodo_records
    ]


@app.task(base=N4ETask, bind=True)
def search(self: app.task):
    for concept_doi in get_harvesting_record_ids():
        upsert.apply_async([concept_doi], queue=Sources.ZENODO.name)
