"""
Functions to harvest metadata from the earth data index
"""

import logging
import requests

from kh_populator.celery_deploy import app
from kh_populator.util import Sources
from kh_populator.n4e_task import N4ETask
from kh_populator.harvesting_consumers.datahub_consumer import (
    upsert_dataset,
)

BASE_URL = "https://o2a-data.de/index/rest/search"
SEARCH_QUERY = {
    "indices": ["harvest", "harvest-extern"],
    "query": "*",
    "hits": 100,
    "facets": ["persons.author.fullname"],
    "excludes": [
        "keywords.region",
        "keywords.region_fao",
        "keywords.region_gebco",
        "keywords.region_iho",
        "keywords.region_seavox",
        "keywords.vmap_political",
        "keywords.vmap_vegetation",
        "keywords.vmap_water",
    ],
    "scroll": "true",
}

logger = logging.getLogger(__name__)


@app.task(base=N4ETask, bind=True)
def search(self: app.task, limit: int = 0):

    response = requests.post(BASE_URL, json=SEARCH_QUERY)
    if response.status_code != 200:
        error_msg = (
            "Datahub search request returned response "
            + f"{response.status_code}: {response.text}"
        )
        raise ValueError(error_msg)
    j = response.json()
    counter = 0

    count_per_type = {}
    while j["hits"] > 0 and j["scroll"]:
        if limit > 0 and counter >= limit:
            break
        for record in j["records"]:
            if limit > 0 and counter >= limit:
                continue
            title = "no title"
            if "title" in record:
                title = record["title"]
            gen_type = "unkown"
            if "genericType" in record:
                gen_type = record["genericType"]
            if gen_type not in ["data", "service"]:
                # ["data", "service", "software"]
                # only harvest data for now
                continue
            counter += 1
            if gen_type not in count_per_type:
                count_per_type[gen_type] = 1
            else:
                count_per_type[gen_type] += 1
            print(
                f'{counter}, from {j["totalHits"]}, ' + f"{gen_type}, {title}"
            )

            if limit > 0 and counter >= limit:
                continue
            upsert_dataset.apply_async(
                [record],
                queue=Sources.DTHB.name,
            )

        SEARCH_QUERY["scroll"] = j["scroll"]

        response = requests.post(BASE_URL, json=SEARCH_QUERY)
        j = response.json()
    print(f"Finished, datahub harvested per type: {count_per_type}")
