"""
Pipeline which adds metadata from the CSV tables in this package to
the selected target.
"""

import logging
from typing import Optional

from pandas import DataFrame
from rdflib import URIRef, BNode

from n4e_kh_schema_py.n4eschema import (
    Aggregator,
    Organization,
    Registry,
    Repository,
    Service,
)

from kh_populator.util import (
    Sources,
    RE3DATA_SOURCE_SYSTEM,
    ROR_SOURCE_SYSTEM,
    N4E_UPLOADER_SOURCE_SYSTEM,
    get_overview_infrastructures_dataframe,
    get_id,
)
from kh_populator.harvesting_consumers.re3data_consumer import (
    upsert as re3data_upsert,
)
from kh_populator.harvesting_consumers.ror_consumer import (
    upsert as ror_upsert,
)
from kh_populator.targets.target_handler import TargetHandler
from kh_populator_domain import n4edata
from kh_populator_domain import re3data
from kh_populator_domain import ror
from kh_populator_logic.rdf import jsonld_dict_to_metadata_object

log = logging.getLogger(__name__)


def get_or_create_organization_from_table(
    ror_id: str,
    short_name: str,
    organizations: DataFrame,
    source_system_ror: URIRef,
    target_handler: TargetHandler,
) -> URIRef:
    """
    Give the short_name if ror_id is not available.
    Either ror_id or short_name must be passed as non-empty strings
    """
    if not ror_id and not short_name:
        raise ValueError("Either ror_id or short_name must be given")
    if ror_id and short_name:
        raise ValueError("Either ror_id or short_name must be empty")
    kh_organization: Optional[URIRef] = URIRef("")
    if ror_id:
        # TODO: should we also handle organization hierarchy
        # (eg. subOrganizationOf)
        # as given by ROR?
        ror_id = ror_id.strip().lstrip("ROR:")
        ror_id = ror_id.lstrip("https://ror.org/")
        ror_id = ror_id.lstrip("http://ror.org/")
        kh_organization = target_handler.get_organization_by_rorid(ror_id)
        if not kh_organization:
            organization = ror.harvest_organization_core(
                target_handler, ror_id, source_system_ror
            )
            if organization:
                kh_organization = target_handler.create(organization)
    else:
        row = organizations[organizations["short"] == short_name].iloc[0]
        name = row["name"]
        kh_organization = Organization(id=BNode(), name=name)
        n4edata.addOrganizationCore(kh_organization, row)
    return kh_organization


def run_pipeline(target_handler: TargetHandler) -> None:
    # ---- Step 1: add collected registries, repositories, aggregators ----
    services = get_overview_infrastructures_dataframe("Gesamt")
    organizations = get_overview_infrastructures_dataframe("Organizations")

    missing_re3data_repo_ids = []
    for _, row in services.iterrows():
        re3data_doi = row["re3data DOI"]
        class_value = row[
            "Class (aggregator, registry, repository, lab instrument, "
            + "Web map application, HPC)"
        ]
        skip_msg = "Skipping entry %s because no class maping: %s" % (
            row["Title"],
            class_value,
        )
        if not isinstance(class_value, str):
            print(skip_msg)
            continue
        if isinstance(re3data_doi, str) and len(re3data_doi) > 0:
            re3data_doi = re3data_doi.strip()
            # currently in the table we always use the re3data doi,
            # but in the KH for the unique sourceSystemID the re3data_id
            # is used
            re3data_link = re3data.resolve_re3data_doi(re3data_doi)
            re3data_id = re3data_link.replace(
                re3data.RE3DATA_BASE_URL + "/repository/", ""
            )
            # re3data repos might be mapped to type Repository or
            # type Aggregator, first try type Repository
            iri = get_id(RE3DATA_SOURCE_SYSTEM, re3data_id)
            if not target_handler.exists(iri):
                re3data_upsert.apply_async(
                    [re3data_id], queue=Sources.RE3DATA.name
                )
                missing_re3data_repo_ids.append(re3data_id)
                continue
            if iri:
                jsonldrecord = target_handler.get_as_jsonld_frame(iri)
                if jsonldrecord["@type"].split("/")[-1] == "Repository":
                    _class = Repository
                else:
                    _class = Aggregator
                catalog = jsonld_dict_to_metadata_object(jsonldrecord, _class)
                n4edata.add_catalog_additional_metadata(catalog, row)
                target_handler.update(catalog)
        else:
            title = row["Title"]
            print("Importing from table: " + title)
            class_value = row[
                "Class (aggregator, registry, repository, lab instrument, "
                + "Web map application, HPC)"
            ]
            skip_msg = "Skipping entry because no class maping: %s" % title
            if not isinstance(class_value, str):
                print(skip_msg)
                continue
            instance_type = class_value.lower()
            if instance_type == "service":
                catalog = Service(id="tmp", name=title)
                n4edata.add_service_core_metadata(catalog, row)

            else:
                if instance_type == "repository":
                    catalog = Repository(id="tmp", title=title, publisher="")
                elif instance_type == "aggregator":
                    catalog = Aggregator(id="tmp", title=title, publisher="")
                elif instance_type == "registry":
                    catalog = Registry(id="tmp", title=title, publisher="")
                else:
                    print(skip_msg)
                    continue
                n4edata.add_catalog_core_metadata(catalog, row, organizations)
                # TODO: we will need sourceSystemID for catalogs
                # from the table!
                n4edata.add_catalog_additional_metadata(catalog, row)
                skip_due_to_not_yet_created_publishers = False
                publisher_iris = []
                for publisher in catalog.publisher:
                    if publisher.hasRorId:
                        kh_iri = get_id(
                            ROR_SOURCE_SYSTEM,
                            publisher.hasRorId,
                        )
                        if not target_handler.exists(kh_iri):
                            ror_upsert.apply_async(
                                [publisher.hasRorId, True],
                                queue=Sources.ROR.name,
                            )
                            skip_due_to_not_yet_created_publishers = True
                            continue
                        else:
                            publisher_iris.append(kh_iri)
                    else:
                        publisher_iris.append(publisher)

                if skip_due_to_not_yet_created_publishers:
                    continue
                catalog.publisher = publisher_iris
                n4edata.add_catalog_additional_metadata(catalog, row)

            catalog.sourceSystem = N4E_UPLOADER_SOURCE_SYSTEM
            iri = get_id(N4E_UPLOADER_SOURCE_SYSTEM, catalog.sourceSystemID)
            catalog.id = iri
            if target_handler.exists(iri):
                target_handler.update(catalog)
            else:
                target_handler.create(catalog)

    if len(missing_re3data_repo_ids) > 0:
        print(
            "Re3data metadata is being harvested - please wait a few minutes "
            + "repeat this pipeline to finalize."
        )
        print(missing_re3data_repo_ids)
        exit()
    print("Pipeline finished successfully")
