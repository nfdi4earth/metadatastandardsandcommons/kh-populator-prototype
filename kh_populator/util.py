import json
import logging
from enum import Enum

from configparser import ConfigParser
import pkgutil
import pandas as pd

from rdflib import URIRef
from typing import Dict


logger = logging.getLogger(__name__)


def _startup(log_level=logging.INFO, log_file=None):
    """Simple startup functionality, mainly to initialise logging"""
    log_config = dict(
        level=log_level,
        format="%(asctime)s %(name)-10s %(levelname)-4s %(message)s",
    )
    if log_file:
        log_config["filename"] = log_file

    logging.basicConfig(**log_config)
    logging.getLogger("").setLevel(log_level)


def config(section, parameter, default=None):
    """Initialise pythons built-in ConfigParser"""

    _config = ConfigParser()
    config_file = pkgutil.get_data(__name__, "config.ini")
    try:
        _config.read_string(config_file.decode("utf-8"))
    except Exception as e:
        logger.error(e)
        raise ImportError("config.ini not found") from e
    if section not in _config:
        print(_config.keys())
        print(dir(_config))
        logger.warning("Section: %s not in *.ini -> using default", section)
        return default
    config_val = _config[section].get(parameter)
    if not config_val:
        logger.warning(
            "Parameter: %s not in section [%s]: of *.ini -> using default",
            parameter,
            section,
        )
        return default
    else:
        return config_val


def get_overview_infrastructures_dataframe(sheet_name: str) -> pd.DataFrame:
    # Loading data from 'inside' of the package
    # ToDo: Enable code to load external files and
    # maybe hold this as exemplary data
    path = "data/Overview N4E infrastructures_Version_2024.xlsx"
    data = pkgutil.get_data(__name__, path)
    return pd.read_excel(data, sheet_name=sheet_name)


def get_edu_train_dataframe() -> pd.DataFrame:
    # Loading data from 'inside' of the package
    # ToDo: Enable code to load external files and
    # maybe hold this as exemplary data
    path = "data/OER_EduTrain_KH_20240829.xlsx"
    try:
        data = pkgutil.get_data(__name__, path)
    except ValueError:
        raise ValueError(
            "An error occurred loading the EduTrain table of learning "
            "resource. Please download and locate it under the path:  %s"
            % path
        )
    result = pd.read_excel(data, sheet_name="All")
    if "Unnamed: 35" in result.columns:
        result.rename(columns={"Unnamed: 35": "Source"}, inplace=True)
    if "Unnamed: 39" in result.columns:
        result.rename(columns={"Unnamed: 39": "Requirement"}, inplace=True)
    for x in [
        "Intro\nBeginner\u200e",
        "BSc.\nBeginner\u200e",
        "MSc.\nIntermediate\u200e",
        "PhD\nAdvanced \u200e",
    ]:
        result.rename(
            columns={x: x.replace("\n", "-").replace("\u200e", "").strip()},
            inplace=True,
        )
    return result


class Sources(Enum):
    RDAMSC = 1
    RE3DATA = 2
    ROR = 3
    ORCID = 4
    WIKIDATA = 5
    N4E = 6
    LHB = 7
    RSD1 = 8
    RSD2 = 9
    ZENODO = 10
    OPENEDX = 11
    ISOGMD = 12
    GDI = 13
    DTHB = 14
    PUB = 16


def get_id(source_system_url: URIRef, source_system_id: str) -> URIRef:
    source_key = SOURCE_SYSTEMS_INDEX[source_system_url]
    prefix = HARVESTERS_CONFIG[source_key]["prefix"]
    # NOTE: preferably use urllib.parse.quote here, however when creating
    # objects in CORDRA, currently the handle can only be passed as URL param.
    # Therefore, if the suffix is encoded, CORDRA will store the decoded id,
    # which leads to bugs when there are special characters. Therefore,
    # manually replace these special characters when they are part of the
    # source_system_id.
    # NOTE that this may lead to two different identifiers becoming one,
    # (e.g. "a:b" and "a-b" will be the same object). However, usage of these
    # special characters in source system identifiers is not probable.
    # Change this to using URL quoting in the future if possible.
    suffix = (
        source_system_id.replace("/", "-")
        .replace(" ", "")
        .replace(":", "-")
        .replace("+", "-")
    )
    return URIRef(f"{cordra_base_url}/objects/n4e/{prefix}-{suffix}")


HARVESTERS_CONFIG = {}
SOURCE_SYSTEMS_INDEX: Dict[URIRef, Sources] = {}
cordra_base_url = config(
    "cordra",
    "base_url",
    default="https://nfdi4earth-knowledgehub.geo.tu-dresden.de/api",
)

harvesters_cfg_filename = "harvesters-config.json"
harvesters_config = pkgutil.get_data(__name__, harvesters_cfg_filename)
if not harvesters_config:
    raise ImportError(f"{harvesters_cfg_filename} not found")
config_dict = json.loads(harvesters_config.decode("utf-8"))
for queuename, config_ in config_dict.items():
    print(
        "Setting up config for queue %s from file %s."
        % (queuename, harvesters_cfg_filename)
    )
    HARVESTERS_CONFIG[Sources[queuename]] = config_
    source_system_iri = URIRef(cordra_base_url + "/objects/" + config_["id"])
    HARVESTERS_CONFIG[Sources[queuename]][
        "sourceSystemIRI"
    ] = source_system_iri
    SOURCE_SYSTEMS_INDEX[source_system_iri] = Sources[queuename]

RDA_MSC_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.RDAMSC]["sourceSystemIRI"]
RE3DATA_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.RE3DATA]["sourceSystemIRI"]
ROR_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.ROR]["sourceSystemIRI"]
ORCID_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.ORCID]["sourceSystemIRI"]
WIKIDATA_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.WIKIDATA]["sourceSystemIRI"]
N4E_UPLOADER_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.N4E]["sourceSystemIRI"]
LHB_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.LHB]["sourceSystemIRI"]
RSD_SOURCE_SYSTEM1 = HARVESTERS_CONFIG[Sources.RSD1]["sourceSystemIRI"]
RSD_SOURCE_SYSTEM2 = HARVESTERS_CONFIG[Sources.RSD2]["sourceSystemIRI"]
ZENODO_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.ZENODO]["sourceSystemIRI"]
OPENEDX_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.OPENEDX]["sourceSystemIRI"]
DATAHUB_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.DTHB]["sourceSystemIRI"]
GDI_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.GDI]["sourceSystemIRI"]
PUB_SOURCE_SYSTEM = HARVESTERS_CONFIG[Sources.PUB]["sourceSystemIRI"]


def format_json(json_dict: dict, indent: int = 4) -> str:
    """Utility function to format JSON to a string for readable printing."""
    if json_dict is None:
        return "{} (None)"
    return json.dumps(json_dict, indent=indent)


def get_custom_schema(schema_file_name: str) -> Dict:
    path = "custom_schemas/" + schema_file_name
    schema_txt = pkgutil.get_data(__name__, path)
    return json.loads(schema_txt)
