import frontmatter
from rdflib import URIRef

from n4e_kh_schema_py.n4eschema import (
    LHBArticle,
    RDMRole,
    Person,
    ResearchProject,
    Publication,
    Dataset,
)
from kh_populator.pipelines.harvest_incub_and_pilots import (
    harvest_individual_research_project,
)
from kh_populator_domain.gitlab_source import metadata_to_lhb_article


# NOTE:
# Currently works only as `pytest tests/domain/test_gitlab_source.py` from the
# package root because of the relative import (see below)
def test_lhb_record_to_n4e():
    # TODO: handle as relative import?
    path = "tests/data/lhbarticle.md"
    with open(path) as file:
        md_text = file.read()
    parsed = frontmatter.loads(md_text)
    metadata = parsed.metadata
    article = metadata_to_lhb_article(metadata)
    for author in article.author:
        author.id = "tmp"
    control_article = LHBArticle(
        id="tmp",
        name=["Dokumentation des NFDI4Earth Living Handbook"],
        additionalType=["collection"],
        description=[
            "Collection der Artikel über das NFDI4Earth Living Handbook."
        ],
        audience=[
            RDMRole(
                "general public",
            )
        ],
        author=[
            Person(
                name=["Thomas Rose"],
                id="tmp",
                orcidId="http://orcid.org/0000-0002-8186-3566",
            )
        ],
        inLanguage=[
            URIRef(
                "http://publications.europa.eu/resource/authority/language/DEU"
            )
        ],
        keywords=["NFDI4Earth", "Living Handbook"],
        license=[URIRef("http://spdx.org/licenses/CC-BY-4.0")],
        subjectArea=[
            URIRef("http://vocabularies.unesco.org/thesaurus/mt2.35"),
            URIRef("http://vocabularies.unesco.org/thesaurus/concept8079"),
            URIRef("http://vocabularies.unesco.org/thesaurus/concept503"),
        ],
        version="1.0",
    )

    assert control_article == article


def test_harvest_individual_research_project():
    pr = harvest_individual_research_project(
        88834, URIRef("http://project"), "pilot"
    )
    pr_test = ResearchProject(
        id="tmp",
        name=["World Settlement Footprint (WSF)"],
        sourceSystemID="88834",
        sourceSystemURL="https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/pilots/WSF",
    )
    pr_test.additionalType = "pilot"
    pr_test.funder = URIRef("http://project")
    pr_test.sourceSystem = URIRef(
        "http://localhost:8080/objects/n4ekh/0550c8e5b1a0caddf73a"
    )
    pr_test.hasOutcome = [
        Publication(
            name=[""],
            id="tmp",
            identifier=["10.5281/zenodo.7858700"],
        ),
        LHBArticle(
            name="",
            url="https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/blob/main/docs/Pilot_WSF.md",
            id="tmp",
        ),
        Dataset(
            title="",
            id="tmp",
            landingPage="https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/WSF_2015",
        ),
        Dataset(
            title="",
            id="tmp",
            landingPage="https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/WSF_2019",
        ),
        Dataset(
            title="",
            id="tmp",
            landingPage="https://geoservice.dlr.de/eoc/ogc/stac/v1/collections/WSF_Evolution",
        ),
    ]
    assert pr == pr_test
