import requests
from rdflib import Literal, URIRef
from n4e_kh_schema_py.n4eschema import (
    Repository,
    Organization,
    MetadataStandard,
    Kind,
    API,
)

from kh_populator_domain.re3data import (
    harvest_catalog,
)


def test_harvest_catalog():
    test_record_id = "r3d100010299"
    catalog = harvest_catalog(test_record_id)
    print(catalog)
    catalog_test = Repository(
        title=[Literal("World Data Center for Climate", lang="en")],
        contactPoint=[
            Kind(hasEmail=["data@dkrz.de"]),
            Kind(hasEmail=["lammert@dkrz.de"]),
        ],
        description=[
            Literal(
                "The mission of World Data Center for Climate (WDCC) is to provide central support for the German and European climate research community. The WDCC is member of the ISC's World Data System. Emphasis is on development and implementation of best practice methods  for Earth System data management. Data for and from climate research are collected, stored and disseminated. The WDCC is  restricted to data products. Cooperations exist with thematically corresponding data centres of, e.g., earth observation,  meteorology, oceanography, paleo climate and environmental sciences. The services of WDCC are also available to external  users at cost price. A special service for the direct integration of research data in scientific publications has been developed. The editorial process at WDCC ensures the quality of metadata and research data in collaboration with the data producers. A citation code and a digital identifier (DOI) are provided and registered together with citation information at the DOI registration agency DataCite.",
                lang="en",
            )
        ],
        keyword=[
            "CERA database",
            "FAIR",
            "climate",
            "climate simulation",
            "cryosphere",
            "data assimilation",
            "earth science",
            "earth system sciences",
        ],
        language=[
            URIRef(
                "http://publications.europa.eu/resource/authority/language/DEU"
            ),
            URIRef(
                "http://publications.europa.eu/resource/authority/language/ENG"
            ),
        ],
        modified="2025-02-18",
        publisher=[
            Organization(
                id="tmp",
                name=[Literal("German Climate Computing Centre")],
                hasRorId="03ztgj037",
                homepage=["https://www.dkrz.de/en"],
            ),
            Organization(
                id="tmp",
                name=[Literal("World Data System")],
                hasRorId="01pwmqe95",
                homepage=["https://worlddatasystem.org/"],
            ),
        ],
        subjectArea=[
            URIRef("https://github.com/tibonto/dfgfo/3"),
            URIRef("https://github.com/tibonto/dfgfo/313"),
            URIRef("https://github.com/tibonto/dfgfo/313-01"),
            URIRef("https://github.com/tibonto/dfgfo/313-02"),
            URIRef("https://github.com/tibonto/dfgfo/314"),
            URIRef("https://github.com/tibonto/dfgfo/34"),
        ],
        id="tmp",
        altLabel=[Literal("DKRZ long term archive"), Literal("WDCC")],
        contentType=[
            "Archived data",
            "Databases",
            "Images",
            "Plain text",
            "Scientific and statistical data formats",
            "Standard office documents",
            "Structured graphics",
            "Structured text",
        ],
        hasAPI=[
            API(
                endpointURL="https://www.wdc-climate.de/docs/GUI_Doku.pdf",
                conformsTo="REST",
            )
        ],
        homepage=[URIRef("https://www.wdc-climate.de/ui/")],
        assignsIdentifierScheme=[
            URIRef("http://purl.org/spar/datacite/doi"),
            URIRef("http://purl.org/spar/datacite/handle"),
        ],
        catalogAccessType="open",
        dataAccessType=["open", "restricted"],
        dataAccessRestriction=["registration"],
        dataLicense=[
            URIRef(
                "http://creativecommons.org/licenses/by-nc-sa/2.0/de/deed.en"
            ),
            URIRef("https://www.wdc-climate.de/ui/info?site=termsofuse"),
        ],
        repositoryType=["disciplinary"],
        hasCertificate=["other"],
        dataUploadRestriction=["registration"],
        dataUploadType="restricted",
    )
    catalog_test.supportsMetadataStandard = [
        MetadataStandard(
            title=["DIF - Directory Interchange Format"],
            id="tmp",
            hasDocument="http://www.dcc.ac.uk/resources/metadata-standards/dif-directory-interchange-format",
        ),
        MetadataStandard(
            title=["DataCite Metadata Schema"],
            id="tmp",
            hasDocument="http://www.dcc.ac.uk/resources/metadata-standards/datacite-metadata-schema",
        ),
        MetadataStandard(
            title=["ISO 19115"],
            id="tmp",
            hasDocument="http://www.dcc.ac.uk/resources/metadata-standards/iso-19115",
        ),
        MetadataStandard(
            title=["Repository-Developed Metadata Schemas"],
            id="tmp",
            hasDocument="http://www.dcc.ac.uk/resources/metadata-standards/repository-developed-metadata-schemas",
        ),
    ]

    for org in catalog.publisher:
        org.id = "tmp"
    for standard in catalog.supportsMetadataStandard:
        standard.id = "tmp"
    assert catalog_test == catalog
