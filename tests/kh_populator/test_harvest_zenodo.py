from kh_populator.harvesting_searchers.zenodo_searcher import (
    get_harvesting_record_ids,
)


def test_get_harvesting_record_ids():
    zenodo_concept_dois = get_harvesting_record_ids()
    assert len(zenodo_concept_dois) > 0
    assert zenodo_concept_dois[0].startswith("10.5281/")
