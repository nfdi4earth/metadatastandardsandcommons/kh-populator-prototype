""" Utility functions for testing data validity """

import re
from n4e_kh_schema_py.n4eschema import PeriodOfTime
from dateutil import parser


def validate_period_of_time(l_period_of_time: PeriodOfTime):
    """
    Tests whether start date < end date
    """
    try:
        for period_of_time in l_period_of_time:
            start_date = parser.parse(period_of_time.startDate)
            if period_of_time.endDate:
                # startDate might be given but no endDate in case of
                # ongoing time series
                end_date = parser.parse(period_of_time.endDate)
                if end_date < start_date:
                    return False
        return True
    except parser.ParserError:
        return False


def validate_email(email):
    email_re = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"
    match = re.findall(email_re, email)
    if match:
        return match[0]
    return None
