"""
These functions are responsible for harvesting organization metadata from
organizations registered at ror.org.
"""

import logging

from rdflib import URIRef, Literal

import requests
from typing import Dict

from n4e_kh_schema_py.n4eschema import Organization, Geometry

from kh_populator_domain import dcatde
from kh_populator_domain import geonames, wikidata
from kh_populator_logic.rdf import ns_geonames
from kh_populator_logic.geometry import lat_lng_to_wkt


log = logging.getLogger(__name__)

# Attention: we make use of the non-stable beta API
BASE_URL = "https://api.ror.org/organizations/"
BASE_URL_UI = "https://ror.org/"


def ror_url_to_id_str(ror_url: str):
    return ror_url.replace("https://ror.org/", "")


def parse_organization_dict(ror_dict: Dict) -> Organization:
    name = ror_dict["name"]
    log.info("Harvesting from ROR: %s", name)
    orga = Organization(id="tmp", name=Literal(name))
    # according to the current ROR documentation (05-2023), the primary name
    # field usually defaults to English but may also be another language.
    # This is most likely going to change in the future, but for the moment
    # we cannot set the language tag of the primary name with certainty.
    orga.hasRorId = ror_url_to_id_str(ror_dict["id"])
    # todo handle 0-N r3d:additionalName
    # r3d:description cardinality: 0-1
    links = ror_dict["links"]
    if len(links) != 1:
        print(
            "--- Found less/more than one link for organization from ROR ---"
        )
        # pdb.set_trace()
    else:
        orga.homepage = [URIRef(link) for link in links]
    if "Wikidata" in ror_dict["external_ids"]:
        identifier_dict = ror_dict["external_ids"]["Wikidata"]
        wikidata_id = identifier_dict["preferred"]
        if wikidata_id is not None:
            uri = wikidata.wikidata_id_to_uri(wikidata_id)
            if uri not in orga.sameAs:
                orga.sameAs.append(uri)
        elif "all" in identifier_dict:
            for wikidata_id_unclassified in identifier_dict["all"]:
                uri = wikidata.wikidata_id_to_uri(wikidata_id_unclassified)
                if uri not in orga.sameAs:
                    orga.sameAs.append(uri)

    if "country" in ror_dict:
        orga.countryName = ror_dict["country"]["country_name"]
    if "addresses" in ror_dict:
        for address in ror_dict["addresses"]:
            if "lat" in address and "lng" in address:
                wkt_literal = lat_lng_to_wkt(address["lng"], address["lat"])
                orga.hasGeometry.append(Geometry(asWKT=wkt_literal))
            if "city" in address:
                orga.locality.append(address["city"])
            if "geonames_city" in address:
                geonames_city = address["geonames_city"]
                city_geonames_id = geonames_city["id"]
                orga.hasLocality.append(
                    URIRef(ns_geonames + str(city_geonames_id) + "/about.rdf"),
                )

                if (
                    "geonames_admin1" in geonames_city
                    and orga.countryName == "Germany"
                ):
                    state_name = None
                    try:
                        state_geonames_id = geonames_city["geonames_admin1"][
                            "id"
                        ]
                        state_name = geonames.get_state_name_from_id(
                            state_geonames_id
                        )
                    except Exception as e:
                        print(
                            "Exception in trying to derive geonames state id"
                        )
                        e.with_traceback()
                    if state_name:
                        state_dcatde_uri = dcatde.map_state_de_to_dcatde(
                            state_name
                        )
                        if state_dcatde_uri:
                            orga.politicalGeocodingURI.append(state_dcatde_uri)
    if "acronyms" in ror_dict:
        for acr in ror_dict["acronyms"]:
            orga.altLabel.append(Literal(acr))
    if "labels" in ror_dict:
        for label_dict in ror_dict["labels"]:
            label = Literal(label_dict["label"])
            if "iso639" in label_dict:
                label_name = label_dict["label"]
                label_lang = label_dict["iso639"]
                existing_name_in_same_lang = [
                    name for name in orga.name if name.language == label_lang
                ]
                if len(existing_name_in_same_lang) == 1:
                    orga.name.remove(existing_name_in_same_lang[0])
                    orga.altLabel.append(existing_name_in_same_lang[0])
                orga.name.append(Literal(label_name, lang=label_lang))
            else:
                orga.altLabel.append(label)
    return orga


def get_rorid_for_external_id(external_id: str, identifier_type: str) -> str:
    """
    Makes a query to the ROR API to check if a ROR record exists with the
    unique external identifier
    :param str external_id: the external identifier
    :param str identifier_type: the name of the external identifier type as in
        the ROR record, e.g. "GRID" or "FundRef"
    :return: the ROR identifier if found or an empty string
    :rtype: str
    """
    url = BASE_URL[:-1] + '?query="' + external_id + '"'
    # in theorey this returns an exact match if a ROR organization has the
    # requested id listed under the external_ids field
    response = requests.get(url)
    if response.status_code == 200:
        result = response.json()
        if result["number_of_results"] > 1:
            log.warning(
                "ROR returned several records from ROR for external id %s: %s"
                % (identifier_type, external_id)
            )
        elif result["number_of_results"] == 1:
            # However to make sure we additionally check the result
            found_ror_record = result["items"][0]
            if (
                "external_ids" in found_ror_record
                and identifier_type in found_ror_record["external_ids"]
            ):
                identifier = found_ror_record["external_ids"][identifier_type]
                if "all" in identifier:
                    all = identifier["all"]
                    if (isinstance(all, str) and external_id == all) or (
                        isinstance(all, list) and external_id in all
                    ):
                        return found_ror_record["id"]
    return ""
