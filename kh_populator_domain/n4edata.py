# -*- coding: utf-8 -*-
# Copyright (C) 2022 jonas.grieb@senckenberg.de

import logging
from typing import List
from geomet import wkt
from pandas import isnull as pd_isnull, Series, DataFrame
from rdflib import URIRef, Literal, BNode
from requests.exceptions import ConnectTimeout

from n4e_kh_schema_py.n4eschema import (
    Catalog,
    Kind,
    Location,
    Organization,
    Service,
)
from kh_populator_logic.rdf import get_dfgfo_subject_for_literal
from kh_populator_domain import geonames, ror
from kh_populator_logic.validity_tests import validate_email

log = logging.getLogger(__name__)


def add_catalog_core_metadata(
    instance: Catalog, row: Series, organizations: DataFrame
) -> None:
    """
    :param instance: An instance of Repository, Aggregator or Registry
    :param row: A row of a pandas Dataframe
    :return: None
    Adds core metadata fields from the row to the instance, which typically
    is a subclass of Catalog.
    """
    instance.title = [Literal(row["Title"].strip(), lang="en")]
    instance.description.append(Literal(row["Description"], lang="en"))
    instance.sourceSystemID = row["id"]
    codes = row["DFG subject areas"]
    if pd_isnull(codes):
        subject_areas = []
        for code in codes.strip().split(";"):
            if code.strip().lower() == "mix":
                continue
            try:
                code = code.strip().replace("_x000D_\n", "")
                dfgfo_subject = get_dfgfo_subject_for_literal(code.strip())
                if dfgfo_subject:
                    subject_areas.append(dfgfo_subject)
            except ValueError:
                log.warning(
                    f"Unable to map term to DFG subject: {code.strip()}"
                )
        instance.subjectArea = subject_areas

    publishers: List[Organization] = []
    orga_ror_ids = row["ROR ID"]
    if not pd_isnull(orga_ror_ids) and isinstance(orga_ror_ids, str):
        for ror_id in orga_ror_ids.strip().split(";"):
            if len(ror_id) == 0:
                continue
            ror_id = ror_id.strip()
            if ror_id.startswith("ROR:"):
                ror_id = ror_id.replace("ROR:", "")
            if ror_id.startswith("http://") or ror_id.startswith("https://"):
                ror_id = ror.ror_url_to_id_str(ror_id)
            publishers.append(
                Organization(id="tmp", name="tmp", hasRorId=ror_id)
            )
        # TODO: consider making use of Wikidata ID as alternative identifier:
        # if not pd_isnull(row["Wikidata ID"]):...
        orga_short_names = row["Institute short name if ROR is not avaiable"]
        if not pd_isnull(orga_short_names) and isinstance(
            orga_short_names, str
        ):
            for short_name in orga_short_names.strip().split(";"):
                if short_name == "":
                    continue
                orgrow = organizations[
                    organizations["short"] == short_name.strip()
                ].iloc[0]
                orga = Organization(id=BNode(), name=orgrow["name"])
                add_organization_core_metadata(orga, orgrow)
                publishers.append(orga)
    instance.publisher = publishers
    homepage = row["Weblink"]
    if not pd_isnull(homepage):
        instance.homepage = URIRef(homepage)
    email_str = row["Email"]
    if isinstance(email_str, str) and "@" in email_str:
        if ";" not in email_str:
            emails = [email_str]
        else:
            emails = email_str.strip().split(";")
        for email in emails:
            if len(email) == 0:
                continue
            val_email = validate_email(email)
            if val_email:
                instance.contactPoint.append(Kind(hasEmail=val_email))
    print(instance.contactPoint)


def add_service_core_metadata(instance: Service, row) -> None:
    instance.sourceSystemID = row["id"]
    instance.name = [Literal(row["Title"].strip(), lang="en")]
    instance.description.append(Literal(row["Description"], lang="en"))
    instance.url = URIRef(row["Weblink"])


def add_organization_core_metadata(instance: Organization, row) -> None:
    instance.name = row["name"]
    instance.sourceSystemID = "org-" + row["short"]
    if not pd_isnull(row["homepage"]):
        instance.homepage = URIRef(row["homepage"])


def add_catalog_additional_metadata(instance, row):
    spatialCoverage = row["geographicalExtent"]
    if (
        isinstance(spatialCoverage, str)
        and len(spatialCoverage) > 0
        and spatialCoverage != "not applicable"
    ):
        if (
            spatialCoverage.lower() == "world"
            or spatialCoverage.lower() == "global"
        ):
            bbox_str = (
                "POLYGON ((180 -90, 180 90, -180 90, -180 -90, 180 -90))"
            )
            location = Location(name="global", boundingBox=bbox_str)
            instance.spatialCoverage = location
        else:
            location = Location(name=spatialCoverage)
            instance.spatialCoverage = location
            try:
                geonames_iri = geonames.retrieve_geonames_id_for_name(
                    spatialCoverage
                )
                if isinstance(geonames_iri, str):
                    location.identifier = geonames_iri
                    geonames_id = geonames_iri.replace(
                        "http://sws.geonames.org/", ""
                    ).rstrip("/")
                    if geonames_id.isnumeric():
                        bbox = geonames.retrieve_bbox_for_geonames_id(
                            int(geonames_id)
                        )
                        if isinstance(bbox, dict):
                            e = bbox["east"]
                            w = bbox["west"]
                            n = bbox["north"]
                            s = bbox["south"]
                            bbox_geojson = {
                                "type": "Polygon",
                                "coordinates": [
                                    [[e, s], [e, n], [w, n], [w, s], [e, s]]
                                ],
                            }
                            bbox_wkt = wkt.dumps(bbox_geojson, decimals=4)
                            location.boundingBox = bbox_wkt
            except ConnectTimeout:
                pass
