import xmltodict
import requests
from rdflib import BNode
from n4e_kh_schema_py.n4eschema import Publication, Person, Organization
from kh_populator_logic.language import (
    lang_code_3_to_eu_vocab,
)
from kh_populator_logic.rdf import get_spdx_iri_from_license_url
from dateutil import parser

ZENODO_OAI_BASE_URL = "https://zenodo.org/oai2d"
ZENODO_GET_RECORD_URL = (
    "https://zenodo.org/oai2d?metadataPrefix=oai_datacite&verb="
    + "GetRecord&identifier="
)


def resolve_doi_to_zenodo_id(doi: str) -> dict:
    """
    This function resolves the doi of a zenodo record.
    In case that a concept doi is passed it returns the zenodo record id
    of the latest version of the record.
    E.g. '10.5281/zenodo.8139082' (concept doi) -> '8139083'
    E.g. '10.5281/zenodo.8139083' -> '8139083'

    :param doi: the doi of the zenodo record (e.g. '10.5281/zenodo.12345')
    :return: the zenodo id of the record to which one is resolved
    """
    url = "https://doi.org/" + doi
    response = requests.head(url)
    # TODO: handle case that response is not a 301
    redirect_url = response.headers["location"]
    response = requests.head(redirect_url)
    if response.status_code == 429:
        return {"status": 429}
    redirect_url = response.headers["location"]
    if redirect_url.startswith("/records/"):
        redirect_url = "https://zenodo.org" + redirect_url
    response = requests.head(redirect_url)
    if response.status_code == 429:
        return {"status": 429}
    if response.status_code == 302:
        redirect_url = response.headers["location"]
        latest_zenodo_id = redirect_url.replace("/records/", "")
    else:
        latest_zenodo_id = redirect_url.replace(
            "https://zenodo.org/records/", ""
        )
    return {"status": 200, "latest_id": latest_zenodo_id}


def get_oai_record(oai_record_id):
    url = ZENODO_GET_RECORD_URL + oai_record_id
    oai_response = requests.get(url)
    record = xmltodict.parse(oai_response.text)
    return record["OAI-PMH"]["GetRecord"]["record"]


def fetch_records() -> list:
    records = []
    continue_harvesting = True
    resumption_token = ""
    while continue_harvesting:
        if resumption_token:
            oai_response = requests.get(
                ZENODO_OAI_BASE_URL,
                params={
                    "verb": "ListRecords",
                    "resumptionToken": resumption_token,
                },
            )
        else:
            oai_response = requests.get(
                ZENODO_OAI_BASE_URL,
                params={
                    "verb": "ListRecords",
                    "metadataPrefix": "oai_datacite",
                    "set": "user-nfdi4earth",
                },
            )
        response_dict = xmltodict.parse(oai_response.text)
        list_records_response = response_dict["OAI-PMH"]["ListRecords"]
        if (
            "resumptionToken" not in list_records_response
            or "#text" not in list_records_response["resumptionToken"]
        ):
            continue_harvesting = False
        else:
            resumption_token = list_records_response["resumptionToken"][
                "#text"
            ]
        records += list_records_response["record"]
    return records


def zenodo_object_to_publication(record: dict) -> Publication:
    metadata = record["metadata"]["oai_datacite"]["payload"]["resource"]

    # TODO: Here Issue #74 should be addressed. Allow types other than Article
    # Use resourceType field
    publication = Publication(id="", name="")

    if "identifier" in metadata:
        publication.identifier = metadata["identifier"]["#text"]

    if "titles" not in metadata:
        publication.name = ["Missing title!"]
    else:
        title = metadata["titles"]["title"]
        if isinstance(title, list):
            titles = []
            for t in title:
                if isinstance(t, str):
                    titles.append(t)
                elif isinstance(t, dict):
                    if (
                        "@titleType" in t
                        and t["@titleType"] == "Subtitle"
                        and "#text" in t
                    ):
                        publication.altLabel.append(t["#text"])
            publication.name = titles
        else:
            publication.name = [title]

    # TODO: Implement abstract vs description?
    if "descriptions" in metadata:
        description = metadata["descriptions"]["description"]
        if isinstance(description, list):
            publication.description = [d["#text"] for d in description]
        else:
            publication.description = description["#text"]

    if "creators" in metadata:
        # NOTE: sometimes the creator on Zenodo is "NFDI4Earth Consortium",
        # so not a Person but rather an Organization. Unfortunately, the
        # Zenodo API does not seem to have any flag which distinguishes this,
        # so for the moment we import this createor as a Person
        author = metadata["creators"]["creator"]
        if not isinstance(author, list):
            author = [author]
        for sub_author in author:
            author_name = sub_author["creatorName"]["#text"]
            person = Person(
                id=BNode(),
                name=author_name,
            )
            if "nameIdentifier" in sub_author:
                name_identifier = sub_author["nameIdentifier"]
                if name_identifier["@nameIdentifierScheme"] == "ORCID":
                    person.orcidId = name_identifier["#text"]
            # Avoid KeyErrors when no affil given
            if "affiliation" in sub_author:
                author_affil = sub_author["affiliation"]
                affiliation = None
                if isinstance(author_affil, str):
                    affiliation = Organization(BNode(), name=author_affil)
                elif isinstance(author_affil, dict):
                    if "#text" in author_affil:
                        affiliation = Organization(
                            BNode(), name=author_affil["#text"]
                        )
                    if (
                        "@affiliationIdentifierScheme" in author_affil
                        and author_affil["@affiliationIdentifierScheme"]
                        == "ROR"
                    ):
                        # TODO: we could consider harvesting the organization
                        # into the knowledge hub here and link to the KH
                        # entity IRI instead of creating a BNode
                        ror_id_url = author_affil["@affiliationIdentifier"]
                        ror_id = ror_id_url.replace("https://ror.org/", "")
                        affiliation.hasRorId = ror_id
                if affiliation:
                    person.affiliation.append(affiliation)
            publication.author.append(person)

    if "dates" in metadata:
        for date in metadata["dates"]["date"]:
            if (
                "#text" in date
                and "@dateType" in date
                and date["@dateType"] == "Issued"
            ):
                datetime_object = parser.parse(date["#text"])
                publication.datePublished = datetime_object.date()
    if "language" in metadata:
        language_code = metadata["language"]
        publication.inLanguage = lang_code_3_to_eu_vocab(language_code)
    if "subjects" in metadata:
        subject = metadata["subjects"]["subject"]
        if isinstance(subject, list):
            publication.keywords = subject
        # Specific workaround for 'list as a string'
        # Other special chars won't be implemented
        elif isinstance(subject, str):
            subject_list = subject.split(",")
            publication.keywords = subject_list

    if "rightsList" in metadata:
        rights = metadata["rightsList"]["rights"]
        if isinstance(rights, dict) and "@rightsURI" in rights:
            license_id = rights["@rightsURI"]
            license_uri = get_spdx_iri_from_license_url(license_id)
            if license_uri:
                publication.license = license_uri

    if "contributors" in metadata:
        contributor = metadata["contributors"]["contributor"]
        if isinstance(contributor, list):
            for sub_contributor in contributor:
                contributor_name = sub_contributor["contributorName"]["#text"]
                publication.contributor.append(
                    Person(id=BNode(), name=contributor_name)
                )
        else:
            contributor_name = contributor["contributorName"]["#text"]
            publication.contributor.append(
                Person(id=BNode(), name=contributor_name)
            )

    return publication


def get_concept_doi_for_record(record: dict) -> tuple:
    metadata = record["metadata"]["oai_datacite"]["payload"]["resource"]
    # TODO: See if this type of checking for newest version is prone to errors!
    if "relatedIdentifiers" in metadata:
        relatedIdentifier = metadata["relatedIdentifiers"]["relatedIdentifier"]
        for id_object in relatedIdentifier:
            if id_object["@relationType"] == "IsVersionOf":
                concept_doi = id_object["#text"]
        return concept_doi
