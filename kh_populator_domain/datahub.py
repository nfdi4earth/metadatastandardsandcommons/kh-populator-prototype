"""

"""

import logging
from datetime import datetime
import re
from typing import Optional
import shapely.wkt
from shapely.geometry.polygon import orient
from rdflib import BNode, URIRef
from n4e_kh_schema_py.n4eschema import (
    Resource,
    Dataset,
    Service,
    SoftwareSourceCode,
    Distribution,
    Location,
    Person,
    Organization,
    PeriodOfTime,
)
from kh_populator_logic.rdf import (
    license_id_to_url_based_on_spdx,
    license_name_to_url_based_on_spdx,
)
from kh_populator_logic.validity_tests import validate_period_of_time
from kh_populator.util import get_id, ROR_SOURCE_SYSTEM

log = logging.getLogger(__name__)

# the following is to prevent the creation of many blank nodes in the KH
# triple store
publisher_organization_mappings = {
    # the following mapping is not completely correct, since PANGAEA
    # itself is the publisher however PANGAEA in the KH is considered
    # only of type Repository, not Repository and Organization (publisher
    # expects an organization).
    "PANGAEA - Data Publisher for Earth and Environmental Science": [
        get_id(ROR_SOURCE_SYSTEM, "032e6b942"),
        get_id(ROR_SOURCE_SYSTEM, "04ers2y35"),
    ],
    "World Data Center for Climate": [get_id(ROR_SOURCE_SYSTEM, "03ztgj037")],
}


def datahub_record_to_n4e(record: dict) -> Optional[Resource]:
    # RESOURCE TYPE?
    genericType = record["genericType"]
    if genericType == "data":
        n4e_resource = Dataset(id="tmp", title=record["title"])
    elif genericType == "service":
        n4e_resource = Service(id="tmp", name=record["title"])
    elif genericType == "software":
        n4e_resource = SoftwareSourceCode(id="tmp", name=record["title"])
    else:
        return

    # DESCRIPTION
    if "description" in record:
        description = record["description"]
        if isinstance(description, str) and len(description) > 0:
            n4e_resource.description = description

    # VERSION
    if "version" in record:
        if genericType in ["data"]:
            n4e_resource.version = record["version"]

    # DATE ISSUED
    if "date" in record:
        if genericType in ["data"]:
            date_string = str(datetime.fromisoformat(record["date"]).date())
            if genericType in ["data", "service"]:
                n4e_resource.issued = date_string
            elif genericType == "software":
                n4e_resource.datePublished = date_string

    # DOI - landing page
    if "identifiers" in record:
        if genericType in ["data"]:
            for identifier in record["identifiers"]:
                if identifier["type"] == "doi":
                    doi = identifier["id"]
                    if doi.startswith("https://doi.org/"):
                        n4e_resource.landingPage = doi
                        # while landingPage has a range a Literal typed as uri,
                        # sameAs expects a RDF URIRef
                        n4e_resource.sameAs = [URIRef(doi)]

    # DOWNLOAD URL + landing page if not yet filled
    for resource in record["resources"]:
        if genericType in ["data"]:
            if resource["type"] == "download":
                distribution = Distribution()
                if "title" in resource:
                    distribution.title = [resource["title"]]
                if "uri" in resource:
                    distribution.accessURL = [resource["uri"]]
                # TODO: resource["access"], resource["mimeType"]
                n4e_resource.distribution.append(distribution)
            elif resource["type"] == "link" and not n4e_resource.landingPage:
                n4e_resource.landingPage = resource["uri"]
            if not n4e_resource.landingPage:
                log.warning(
                    "Datahub harvester no landingPage assigned "
                    + f"for {record['title']}"
                )
        if genericType in ["service", "software"]:
            if resource["type"] == "link":
                n4e_resource.url = [resource["uri"]]

    # GEOMETRY
    if (
        "geometry" in record
        and isinstance(record["geometry"], dict)
        and genericType in ["data"]
    ):
        map_spatial_coverage(n4e_resource, record["geometry"])

    # ORGANISATION
    # link dataset to repository in KH
    if "organisations" in record:
        if genericType in ["data"]:
            organisations = record["organisations"]
            if "provider" in organisations:
                providers = organisations["provider"]
                for provider in providers:
                    if "name" in provider:
                        # TODO: how do we handle multiple providers??
                        n4e_resource.originalDataSource = provider["name"]
            if "datacenter" in organisations:
                datacenters = organisations["datacenter"]
                publishers = []
                for datacenter in datacenters:
                    if "name" in datacenter:
                        datacenter_name = datacenter["name"]
                        if datacenter_name in publisher_organization_mappings:
                            publishers.extend(
                                publisher_organization_mappings[
                                    datacenter_name
                                ]
                            )
                        else:
                            publisher = Organization(
                                id="", name=datacenter_name
                            )
                            if "url" in datacenter:
                                publisher.homepage = URIRef(datacenter["url"])
                            publishers.append(publisher)
                n4e_resource.publisher = publishers

    # AUTHORS
    # Sometimes, the author may not be an actual person but an organization
    # e.g., the WSL WOCE Sea Level appears as author
    # How to deal with this?
    if "persons" in record and "author" in record["persons"]:
        for author in record["persons"]["author"]:
            # alternatively, take author_name from fullname
            # format in fullname: lastname, firstname
            author_name = ""
            if "firstname" in author:
                author_name += author["firstname"]
            if "lastname" in author:
                if len(author_name) > 0:
                    author_name += " "
                author_name += author["lastname"]
            if len(author_name) == 0:
                continue

            author_object = Person(id=BNode(), name=author_name)
            if "email" in author and len(author["email"]) > 0:
                author_object.email = author["email"]

            if "orcid" in author["identifiers"]:
                author_object.orcidId = author["identifiers"]["orcid"]

            if "organisation" in author and author["organisation"] != "":
                author_object.affiliation.append(
                    Organization(BNode(), name=author["organisation"])
                )
            if genericType in ["data"]:
                n4e_resource.creator.append(author_object)
            else:
                n4e_resource.author.append(author_object)

    # KEYWORDS
    if "keywords" in record:
        keywords = record["keywords"]
        if "keyword" in keywords:
            if genericType == "data":
                n4e_resource.keyword = [k["name"] for k in keywords["keyword"]]
            else:
                n4e_resource.keywords = [
                    k["name"] for k in keywords["keyword"]
                ]
        # DATA LICENSE
        if "license" in keywords:
            for lic in keywords["license"]:
                license_iris = []
                if "name" in lic:
                    license_iri = license_name_to_url_based_on_spdx(
                        lic["name"]
                    )
                    if license_iri is None:
                        license_iri = license_id_to_url_based_on_spdx(
                            lic["name"]
                        )
                    if license_iri:
                        license_iris.append(license_iri)
                    else:
                        if genericType == "data":
                            pass
                            # TODO: need attribute also for Datasets to
                            # capture license information which does
                            # not conform SPDX standardization
                        elif genericType == "software":
                            n4e_resource.copyrightNotice = lic["name"]
                if len(license_iris) > 0:
                    n4e_resource.license = license_iris

    # TEMPORAL COVERAGE
    if "temporalCoverage" in record and genericType in ["data"]:
        period_of_time = PeriodOfTime(
            startDate=record["temporalCoverage"]["lower"],
            endDate=record["temporalCoverage"]["upper"],
        )
        if validate_period_of_time([period_of_time]):
            n4e_resource.temporal = [period_of_time]

    # MEASURED PARAMETERS
    # Units available too - do not have a counterpart in our schema
    if "parameters" in record and genericType == "data":
        for param in record["parameters"]:
            if "name" in param:
                n4e_resource.variableMeasured.append(param["name"])
    return n4e_resource


def map_spatial_coverage(dataset: Dataset, geometry: dict):
    coords = str(geometry["coordinates"])
    geom_type = geometry["type"]
    regex_p = r"\[(-?\d+\.?\d*),\s*(-?\d+\.?\d*)\]"
    coords_string = (
        re.sub(regex_p, r"\1 \2", coords).replace("[", "(").replace("]", ")")
    )
    if geom_type == "Point":
        wkt = geom_type + "(" + coords_string + ")"
    else:
        wkt = geom_type + coords_string

    geom = shapely.wkt.loads(wkt)
    if (
        geom.geom_type == "Polygon"
    ):  # if necessary, adjust the orientation of the Polygon
        geom = orient(geom)

    if geom.is_valid:
        dataset.spatialCoverage = Location(geometry=geom.wkt)
    else:
        log.info("Invalid geometry")
