"""package setup"""

from setuptools import setup

setup(
    name="kh-populator",
    version="0.1.0",
    description="""
        A unified Python script for protyping the ingestion of metadata and
        transformation to the data model for the Knowledge Hub (KH)
        within NFDI4Earth""",
    author="Jonas Grieb",
    author_email="jonas.grieb@senckenberg.de",
    packages=[
        "kh_populator",
        "kh_populator.pipelines",
        "kh_populator_domain",
        "kh_populator_logic",
    ],
    install_requires=[
        "click",
        "pandas",
        "lxml",
        "requests",
        "rdflib==6.3.2",
        "owslib",
        "pyld",
        "xmltodict==0.13.0",
        "geomet",
        "python-gitlab",
        "python-frontmatter",
        "pyaml",
        "linkml==1.6.11",
        "linkml_runtime==1.6.3",
        "jsonschema>=3.2.0",
        "bs4",
        "celery==5.3",
        "pystac>=1.8.4,<=1.10.1",
        "sickle",
        "shapely==2.0.5",
        "pystac",
        "setuptools"
    ],
    extras_require={
        "dev": [
            "black",
            "flake8",
            "mypy",
            "types-requests",
            "pytest",
            "pytest-console-scripts",
            "requests_mock",
        ],
        "docu": [
            "matplotlib",
        ],
    },
    package_data={
        "kh_populator": ["data/*.tsv"],
    },
    entry_points={
        "console_scripts": [
            "kh_populator = kh_populator.cli:main",
        ]
    },
)
